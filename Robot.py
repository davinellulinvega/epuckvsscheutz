#!/usr/bin/python
# -*- coding: utf-8 -*-
import logging
from random import randint
from scipy.spatial import cKDTree
from math import hypot
from bidict import bidict
import settings
from Cylinder import Cylinder
from Source import Source


class Robot(Cylinder):

    def __init__(self, id_rbt, pos_x=0, pos_y=0, supervisor=None):
        """
        Initialize and declare all the attributes necessary for a robot to sense its environment and maintain its
        homeostasis.
        :param id_rbt: An integer identifying the robot in this simulation.
        :param pos_x: A float representing the robot's initial position along the X axis.
        :param pos_y: A float representing the robot's initial position along the Y axis.
        :param supervisor: An instance of the Supervisor class, managing the current simulation.
        """

        # Initialize the Cylinder parent class, which in turn will initialize the ActorTypeDispatcher
        super(Robot, self).__init__(pos_x=pos_x, pos_y=pos_y, radius=settings.ROBOT_BODY_RADIUS)

        # Initialize two loggers for error and data output
        self._error_log = logging.getLogger("Robots")
        self._data_log = None

        # Initialize the rest of the attributes
        self._dead = False
        self._id = id_rbt
        self._energy = settings.DEFAULT_ROBOT_ENERGY_LEVEL
        self._speed_x = 0.
        self._speed_y = 0.
        self._tendencies = {'action': {'basic': 0, 'current': 0}, 'fight': 0, 'avoid': 0, 'forage': 0}
        self._flee_cycle = 0
        self._action = None
        self._energy_expanded = 0

        # Initialize references to the nearest objects
        self._supervisor = supervisor

    def __eq__(self, other):
        """
        Overload the equality operator.
        :param other: Another instance of the Robot class.
        :return: Boolean. True if both instances have the same id. False otherwise.
        """
        return self._id == other.get_id()

    def __hash__(self):
        """
        Overload the Robot's hashing function.
        :return: The robot's hashed id.
        """
        return hash(self._id)

    def run(self, cycle):
        """
        The robot's main function. It gather information from the environment, decide on the next action and performs
        it.
        In the end both robot's and environment's states are updated.
        :return: Nothing.
        """

        # Gather the environment's state
        sources = self._supervisor.world.get_position_source_map()
        obstacles = self._supervisor.world.get_position_obstacle_map()
        robots_position, robots_act_tendency = self._remove_self(self._supervisor.world.get_position_robot_map(),
                                                                 self._supervisor.world.get_act_tendency_robot_map())

        # Perform the next action
        self._next_action(sources, robots_position, robots_act_tendency, obstacles, cycle)

        # Update both robot's and environment's states
        self._step(cycle)

    def get_id(self):
        """
        Returns the integer identifying this robot.
        :return: Integer.
        """

        return self._id

    def _step(self, cycle):
        """
        Update the robot's state.
        :param cycle: An integer representing the current simulation cycle.
        :return: Nothing.
        """

        # Update the robot's energy level
        self._update_energy()

        # Log the robot's internal state
        # TODO: Modify the stats logged for each robot on each cycle.
        if self._data_log is not None and not self._data_log.closed:
            self._data_log.write('{{"cycle": {}, '
                                 '"energy": {}, '
                                 '"action": "{}", '
                                 '"tendencies": {{"action": {}, '
                                 '"fight": {}, '
                                 '"forage": {}, '
                                 '"avoid": {} }}}}\n'.format(cycle, self._energy, self._action,
                                                             self._tendencies['action']['current'],
                                                             self._tendencies['fight'], self._tendencies['forage'],
                                                             self._tendencies['avoid']))
        if not self._dead:
            # Update the robot's position according to its speeds (linear and angular)
            self._pos_x += self._speed_x
            self._pos_y += self._speed_y

            # Reset the robot's speed to 0 for the next turn
            self._speed_x = 0
            self._speed_y = 0

            # Update the environment
            self._supervisor.world.update_action_tendency_robot_map(self, self._tendencies['action']['current'])
            self._supervisor.world.update_position_robot_map(self, self.get_position())

    def _next_action(self, sources, robots_position, robots_act_tendency, obstacles, cycle):
        """
        Choose and perform the next action, according to the positions of the other objects on the map.
        As well as, update the robot's level of energy.
        :param sources: Dict. The mapping between positions and sources. 
        :param robots_position: Dict. The mapping between positions and robots.
        :param robots_act_tendency: Dict. The mapping between robots and their respective action tendencies.
        :param obstacles: Dict. The mapping between positions and obstacles.
        :param cycle: Int. An integer representing the current simulation cycle.
        :return: Nothing.
        """

        # Check if the robot is still fleeing from a lost battle
        if self._flee_cycle > 0:
            # Well go on fleeing
            self._energy_expanded = self._flee(robots_position, sources, obstacles)
            # Fill in the robot's attribute for logging purposes
            self._action = "flee"

        else:
            # Check if the robot meets the conditions for reproduction
            if cycle >= 250 and self._energy >= settings.PROCREATE_ENERGY_REQUIRED:
                # It procreation time
                self._energy_expanded = self._procreate()
                # Fill in the robot's attribute for logging purposes
                self._action = "procreate"
            else:
                # Check if the nearest robot is in range for a battle
                nearest_robots = self._find_nearest_robot(robots_position, settings.FIGHT_THRES)
                if len(nearest_robots) != 0:
                    # Query the robot for its action tendency
                    if self._is_action_tendency_max(nearest_robots, robots_act_tendency):
                        # Fight
                        self._energy_expanded = self._fight()
                        # Fill in the robot's attribute for logging purposes
                        self._action = "fight"
                    else:
                        # Flee
                        self._energy_expanded = self._flee(robots_position, sources, obstacles)
                        # Fill in the robot's attribute for logging purposes
                        self._action = "flee"
                else:
                    # Check if there are any sources really near
                    nearest_source = self._find_nearest_source(sources)
                    if isinstance(nearest_source, Source) and nearest_source.collide(self.get_position(),
                                                                                     self.get_radius()):
                        # Forage
                        self._energy_expanded = self._forage(nearest_source)

                        # Fill in the robot's attribute for logging purposes
                        self._action = "forage"
                    else:
                        # Move
                        self._energy_expanded = self._move(robots_position, sources, obstacles)
                        # Fill in the robot's attribute for logging purposes
                        self._action = "move"

        # Update the robot's energy level
        self._energy_expanded -= settings.PROCESS_ENERGY_AMOUNT  # Energy is always lost to processing

    def _move(self, robots_pos, sources, obstacles, fleeing=False):
        """
        Computes the robot's speed and direction of movement for the next step.
        Sets the speed_linear and speed_angular, but do not actually update the robot's position.
        :param robots_pos: A bidict mapping a robot's id to its position.
        :param sources: A dict mapping a source's position to the source's instance.
        :param obstacles: A dict mapping an obstacle posotion to its instance.
        :param fleeing: A boolean indicating if the robot is running away or just moving around. Changes the max speed.
        :return: Float. The energy expended for the movement.
        """

        # Initialize the total sum of all the positions
        tot_sum = [0, 0]

        # TODO: Remove all ROBOT_HORIZON from find_nearest_* methods (better or worse performance?)
        # Sum the weighted positions of the sources
        nearest_src = self._find_nearest_source(sources, settings.ROBOT_HORIZON)
        for src in nearest_src:
            # Compute the distance from the source to the robot
            pose = self._transform(src.get_position())
            dist = hypot(pose[0], pose[1])
            # Add the weighted position to the total sum
            tot_sum[0] += (pose[0] / dist ** 2) * self._tendencies['forage']
            tot_sum[1] += (pose[1] / dist ** 2) * self._tendencies['forage']

        # Well basically rinse and repeat for robots and obstacles
        nearest_rbt = self._find_nearest_robot(robots_pos, settings.ROBOT_HORIZON)
        for rbt in nearest_rbt:
            # Compute the distance from the other robot to the robot
            pose = self._transform(robots_pos[rbt])
            dist = hypot(pose[0], pose[1])
            if dist == 0:
                # Add the weighted position to the total sum
                tot_sum[0] += self._tendencies['fight']
                tot_sum[1] += self._tendencies['fight']
            else:
                # Add the weighted position to the total sum
                tot_sum[0] += (pose[0] / dist ** 2) * self._tendencies['fight']
                tot_sum[1] += (pose[1] / dist ** 2) * self._tendencies['fight']

        nearest_obs = self._find_nearest_obstacle(obstacles, settings.ROBOT_HORIZON)
        for obs in nearest_obs:
            # Compute the distance from the obstacle to the robot
            pose = self._transform(obs.get_position())
            dist = hypot(pose[0], pose[1])
            if dist == 0:
                # Add the weighted position to the total sum
                tot_sum[0] += self._tendencies['avoid']
                tot_sum[1] += self._tendencies['avoid']
            else:
                # Add the weighted position to the total sum
                tot_sum[0] += (pose[0] / dist ** 2) * self._tendencies['avoid']
                tot_sum[1] += (pose[1] / dist ** 2) * self._tendencies['avoid']

        # Compute and set the robot's linear speed
        self._speed_x, self._speed_y = tot_sum

        # Check the overall linear speed for all sorts of cases
        linear_speed = hypot(self._speed_x, self._speed_y)
        if fleeing:
            ratio = settings.ROBOT_FLEE_SPEED / linear_speed
            self._speed_x *= ratio
            self._speed_y *= ratio
        else:
            if self._energy <= settings.CRITICAL_ENERGY_LVL and linear_speed > settings.ROBOT_CRIT_SPEED:
                ratio = settings.ROBOT_CRIT_SPEED / linear_speed
                self._speed_x *= ratio
                self._speed_y *= ratio

            if linear_speed > settings.ROBOT_MAX_SPEED:  # Robot is trying to outrun itself
                ratio = settings.ROBOT_MAX_SPEED / linear_speed
                self._speed_x *= ratio
                self._speed_y *= ratio

        # Return the energy expended for this move
        return -(hypot(self._speed_x, self._speed_y) ** 2)

    def _transform(self, pos):
        """
        Transform a position in the World's frame of reference into a position in the Robot's frame of reference.
        :param pos: Tuple. The position in the World's frame of reference.
        :return: Tuple. The position in the Robot's frame of reference.
        """

        # Translate
        tran_x = pos[0] - self._pos_x
        tran_y = pos[1] - self._pos_y

        # Return the transformed position
        return tran_x, tran_y

    def _forage(self, source):
        """
        Forage a certain amount of energy from the nearest source.
        :param source: The nearest instance of a Source class.
        :return: Float. The amount of energy gathered from the source.
        """

        # Forage the nearest energy source if available
        foraged_energy = source.decrease_energy(settings.FORAGE_ENERGY_AMOUNT)

        # Remove the source if empty
        if source.is_empty():
            self._supervisor.world.remove_source(source.get_position())

        # Return the amount of energy foraged.
        # /!\ Return a positive number since it is an increase of energy
        return foraged_energy

    def _fight(self):
        """
        Engage in a battle with the nearest robot.
        :return: Float. The energy expended to fight the other robot.
        """

        # In this simple version of the fight method, we simply set the linear and angular speeds to 0
        self._speed_x = 0.
        self._speed_y = 0.

        # And return the default level of energy expended for fighting
        # /!\ return a negative number since it is  a decrease of energy.
        return -settings.FIGHT_ENERGY_AMOUNT

    def _flee(self, robots_pos, sources, obstacles):
        """
        Update the number of cycles the robot should spend fleeing.
        Calls _move, to get the speed and direction of the fleeing motion.
        :param robots_pos: A bidict containing the mapping between robot id and position.
        :param sources: A dict mapping position to source instances.
        :param obstacles: A dict mapping position to obstacle instances.
        :return: Float. The energy expended for fleeing on the next turn. Should be called until flee_cycle <= 0.
        """

        if self._flee_cycle > 0:
            # Decrease the number of cycles the robot has to flee
            self._flee_cycle -= 1
        else:
            # Choose the number of cycles the robot will flee for.
            self._flee_cycle = randint(settings.MIN_FLEE_CYCLE, settings.MAX_FLEE_CYCLE)

        # Set the fight tendency to a very low level so that the robot avoids any further fight
        old_fight_tendency = self._tendencies['fight']
        self._tendencies['fight'] = -100

        # Move the robot away from everything
        energy_expended = self._move(robots_pos, sources, obstacles, fleeing=True)

        # Restore the fighting tendency to its previous level
        self._tendencies['fight'] = old_fight_tendency

        # Move and return the amount of units lost in fleeing.
        # /!\ return a negative number since it is  a decrease of energy.
        return energy_expended

    def _procreate(self):
        """
        Call the supervisor to create a new robot.
        :return: Float. The energy expended to self-duplicate (this is a SFW process do not worry).
        """

        # Ask the supervisor to create a new robot at a random location on the map
        self._supervisor.add_robot()

        # Return the amount of energy expended for procreation
        return -settings.PROCREATE_ENERGY_AMOUNT

    def _update_energy(self):
        """
        Update the robot's level of energy.
        If the level drops to zero or below, remove the robot from the map, by call to supervisor.
        :return: Boolean. True if every steps in the process went well. False otherwise.
        """

        # Update the level of energy
        self._energy += self._energy_expanded

        # Reinitialize the number of energy units lost
        self._energy_expanded = 0

        # Check if the robot died or not
        if self._energy <= 0:
            # Toggle the dead flag
            self._dead = True
            # Remove the robot from the map
            self._supervisor.remove_robot(self)

    def _find_nearest_source(self, sources, radius=None):
        """
        Find and return a reference to the nearest energy source.
        :param sources: Dict. A mapping between positions and sources.
        :param radius: Float. If specified, the method returns all sources in the area defined by the radius.
        :return: Source/List. If no radius is given, return the nearest Source instance. Else returns a list of all the
        Sources within a certain radius around the Robot instance.
        """

        # If there are no sources
        if len(sources) == 0:
            # Well there  are no nearest sources
            return []

        # Get the sources' position
        src_poses = list(sources.keys())

        # Build the K-D tree which will be used to query the nearest neighbour
        kdt = cKDTree(src_poses, leafsize=10)

        if radius is None:
            # Query the tree for the nearest energy source
            _, src_idx = kdt.query(self.get_position(), k=1)

            # Return the source referred by the index
            return sources[src_poses[src_idx]]
        else:
            # Query the tree for all the sources within the defined radius
            src_idxs = kdt.query_ball_point(self.get_position(), radius)

            # Return the source referred by the index
            return [sources[src_poses[idx]] for idx in src_idxs]

    def _find_nearest_robot(self, robots_pos, radius=None):
        """
        Find and return a reference to the nearest robot.
        :param robots_pos: A bidict mapping robot id to its position.
        :param radius: Float. If given, the method returns a list of all the robots within a radius of the Robot
        instance.
        :return: Robot/List. If no radius is given, returns the nearest Robot instance. Else returns a list of robots 
        within the radius of the Robot instance.
        """

        # If this is the only robot in the simulation
        if len(robots_pos) == 0:
            # There are no other robot
            return []

        # Get the position of the other robots
        rbt_poses = list(robots_pos.values())

        # Build the K-D tree that will be used to look for the nearest neighbour
        kdt = cKDTree(rbt_poses, leafsize=10)

        if radius is None:
            # Query the tree for the nearest neighbour
            _, rbt_idx = kdt.query(self.get_position(), k=1)

            # Return a reference to the nearest neighbour
            return robots_pos.inv[rbt_poses[rbt_idx]]
        else:
            # Query the tree for the nearest neighbour
            rbt_idxs = kdt.query_ball_point(self.get_position(), radius)

            # Return a reference to the nearest neighbour
            return [robots_pos.inv[rbt_poses[idx]] for idx in rbt_idxs]

    def _find_nearest_obstacle(self, obstacles, radius=None):
        """
        Find and return a reference to the nearest obstacle.
        :param obstacles: Dict. A mapping between positions and obstacles.
        :param radius: Float. If given, the method returns a list of all the obstacles within the radius.
        :return: Obstacle/List. If no radius is given, returns the nearest Obstacle instance. Else returns a list of
        Obstacles within the radius of the Robot instance.
        """

        # If there are no obstacles
        if len(obstacles) == 0:
            # There are no nearest obstacles
            return []

        # Get the position of all the obstacles
        obs_poses = list(obstacles.keys())

        # Build the K-D tree that will be used to query the nearest neighbour
        kdt = cKDTree(obs_poses, leafsize=5)

        if radius is None:
            # Query the tree for the nearest obstacle
            _, obs_idx = kdt.query(self.get_position(), k=1)

            # Return a reference to the nearest obstacle
            return obstacles[obs_poses[obs_idx]]
        else:
            # Query the tree for the nearest obstacle
            obs_idxs = kdt.query_ball_point(self.get_position(), radius)

            # Return a reference to the nearest obstacle
            return [obstacles[obs_poses[idx]] for idx in obs_idxs]

    def _remove_self(self, robots_position, robots_act_tendency):
        """
        A simple loop that remove the current Robot instance from the mapping between positions and robots.
        :param robots_position: A bidict mapping a robot id with its position.
        :param robots_act_tendency: A dict mapping a robot id with its action tendency.
        :return: bidict, dict. A copy of the robots' position and action tendency mappings, without the current robot.
        """

        # Make a copy of the mappings
        rbt_pos = bidict(robots_position)
        rbt_act_tend = dict(robots_act_tendency)

        # Remove the position and action tendency corresponding to the current robot
        if len(rbt_pos) > 0:
            rbt_pos.pop(self, None)
        if len(rbt_act_tend) > 0:
            rbt_act_tend.pop(self, None)

        # Return the new mappings
        return rbt_pos, rbt_act_tend

    def _is_action_tendency_max(self, nearest_robots, robots_act_tendency):
        """
        Check with surrounding robots if the Robot's current action tendency is the highest or not.
        :param nearest_robots: A list of ids corresponding to the robots nearby.
        :param robots_act_tendency: A dict. mapping a robot's id to its action tendency.
        :return: Boolean. True is the robot has the highest action tendency. False otherwise.
        """

        # Check if the current action tendency is the highest
        for rbt in nearest_robots:
            if robots_act_tendency[rbt] > self._tendencies['action']['current']:
                # Well it was not the highest
                return False

        # It is the highest
        return True

    def shutdown(self):
        """
        Allow the robot to perform some complementary actions before being destroyed.
        :return: Nothing.
        """

        # If a data log file has been defined
        if self._data_log is not None:
            # Gracefully close the file
            self._data_log.close()
