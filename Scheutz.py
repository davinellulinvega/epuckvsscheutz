#!/usr/bin/python
# -*- coding: utf-8 -*-
from random import gauss
import os
from Robot import Robot
from settings import BASE_DIR, DEFAULT_AVOID_TENDENCY, DEFAULT_FORAGE_TENDENCY


class Scheutz(Robot):
    """
    A specific implementation of the robot class, using Scheutz control scheme.
    """

    def __init__(self, id_rbt, pos_x=0, pos_y=0, supervisor=None):
        """
        Extend the parent's constructor to give specific initial values to the different tendencies.
        """

        # Initialize the parent class
        super(Scheutz, self).__init__(id_rbt, pos_x, pos_y, supervisor)

        # Initialize the data logger
        self._data_log = None

        # Initialize the tendencies
        self._tendencies['action']['basic'] = self._tendencies['action']['current'] = gauss(0.5, 0.125)
        self._tendencies['fight'] = self._tendencies['action']['current'] * 100 - 50
        self._tendencies['forage'] = DEFAULT_FORAGE_TENDENCY
        self._tendencies['avoid'] = DEFAULT_AVOID_TENDENCY

        try:
            self._data_log = open(os.path.join(BASE_DIR, "logs", "RobotData",
                                               "rbt_{}_{}_{}.log".format(supervisor.get_simulation_number(),
                                                                         __name__, id_rbt)), "a")
        except IOError as e:
            self._error_log.exception("Error: Could not open the log file: {}".format(e))

    def _fight(self):
        """
        Extend the parent's _fight method to update the action_tendency and fight_tendency.
        :return: Float. The energy expended during the fight.
        """

        # Update the action tendency
        self._update_action_tendency(win=True)

        # Return the energy expended
        return super(Scheutz, self)._fight()

    def _flee(self, robots_pos, sources, obstacles):
        """
        Extend the parent's _flee method to update the action_tendency and fight_tendency.
        :param robots_pos: A bidict mapping position to robot ids.
        :param sources: A dict mapping position to source instances.
        :param obstacles: A dict mapping position to obstacle instances.
        :return: Float. The energy expended during the fleeing motion.
        """

        # Update the action tendency
        self._update_action_tendency(win=False)

        # Return the expended energy
        return super(Scheutz, self)._flee(robots_pos, sources, obstacles)

    def _update_action_tendency(self, win=True):
        """
        Update the action_tendency and related figth_tendency, depending on the result of a battle.
        :param win: Boolean. Whether the battle was won or not.
        :return: Nothing. Update associated attributes.
        """

        # Get a copy of the current and basic action tendency for the sake of brevity
        r = self._tendencies['action']['basic']
        m = self._tendencies['action']['current']

        # Update the action tendency of the robot
        if win:
            if m >= r + (1 - r) / 2.:
                m = 2 * m - 1
            elif m <= r:
                m /= 2
            else:
                m = r / 2. + r * (m - r) / (1 - r) ** 3
        else:
            if m >= r:
                m += (1 - m) / 2.
            elif m <= r / 2.:
                m *= 2
            else:
                m = r + (2 * m - r) * (1 - r) / (2 * r) ** 2

        # Transfer the values back to the attributes
        self._tendencies['action']['current'] = m

        # Update the conflict tendency of the robot
        self._fight_tendency = 100 * self._tendencies['action']['current'] - 50
