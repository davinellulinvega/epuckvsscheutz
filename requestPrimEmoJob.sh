#!/bin/bash

# Request real and virtual memory
#$ -l mem=64G -l rmem=24G
#
# Ask for an openmpi infini-band environment with 25 cpu
#$ -pe openmp 25
#
# Email notification
#$ -M lcaspar1@sheffield.ac.uk
#
# When to send notifications (beginning, end and abort).
#$ -m bea
#
# Specify the working direcory
#$ -wd /home/acp14lc/Workspaces/EpuckVsScheutz/
#
# Kill it before he names it
#$ -N EpuckVsScheutz

# Load all the modules
module load dev/gcc
module load apps/python/anaconda3-4.2.0
module load apps/python/conda

# Activate the epuckVsScheutz conda environment
source activate epuckVsScheutz

# Launch both scripts
python3 Supervisor.py -f PrimEmo -o 50 -s 50 -r 50 --sims 40
