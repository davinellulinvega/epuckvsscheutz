#!/usr/bin/python
# -*- coding: utf-8 -*-

from settings import OBJECT_RADIUS
from Cylinder import Cylinder


class Obstacle(Cylinder):
    """
    A very simple class to define a cylindrical object that will serve as an obstacle.
    """

    def __init__(self, identifier, pos_x=0.0, pos_y=0.0):
        """
        Initialize the static attributes of an obstacle.
        :param identifier: Unique identifier for the obstacle.
        :param pos_x: Position of the obstacle along the X axis.
        :param pos_y: Position of the obstacle along the Y axis.
        """

        # Initialize the parent class
        super(Obstacle, self).__init__(pos_x, pos_y, radius=OBJECT_RADIUS)

        # Assign the given value to the identifier
        self._id = identifier
