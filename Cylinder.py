#!/usr/bin/python
# -*- coding: utf-8 -*-

from math import hypot


class Cylinder:
    """
    A simple cylindrical object defined by a position and a radius.
    """

    def __init__(self, pos_x, pos_y, radius):
        """
        A declare and initialize the attributes defining the cylinder.
        :param pos_x: The position of the center of mass along the X axis.
        :param pos_y: The position of the center of mass along the Y axis.
        :param radius: A float representing the radius of the cylinder.
        """

        # Initialize any parent class
        super(Cylinder, self).__init__()

        self._pos_x = pos_x
        self._pos_y = pos_y
        self._radius = radius

    def get_position(self):
        """
        A getter for the current position of the cylinder along the X and Y axes.
        :return: Tuple. Representing the current position of the cylinder.
        """

        return self._pos_x, self._pos_y

    def set_position(self, pos_x, pos_y):
        """
        Set the robot's position along both X and Y axis.
        :param pos_x: The robot's position along the X axis.
        :param pos_y: The robot's position along the Y axis.
        :return: Nothing.
        """

        # Set the robot's to the given
        self._pos_x = pos_x
        self._pos_y = pos_y

    def get_radius(self):
        """
        A getter of the radius parameter.
        :return: Float. The radius of the cylinder as configured upon initialization.
        """

        return self._radius

    def collide(self, pos, radius):
        """
        Check if the current instance of the cylinder collides with another cylinder.
        :param pos: Tuple. The position of the colliding cylinder along the X and Y axis.
        :param radius: Float. The radius of the colliding cylinder.
        :return: Boolean. True if there is a collision, False otherwise.
        """

        # Get the features defining the other cylinder
        pos_x, pos_y = pos

        # Compute the distance between the two cylinders
        dist = hypot(pos_x - self._pos_x, pos_y - self._pos_y)

        # Check for collision
        if dist <= (self._radius + radius):
            return True

        # No collision
        return False
