# Description
This project is part of [my PhD thesis](http://etheses.whiterose.ac.uk/28366/) entitled: *"The role of emotions in autonomous social agents"*. The main hypothesis, this thesis explores, is that *"emotions inform the brain as to the nature of a given situation, and guide the decision-making process to increase the chances of survival for a virtual agent"*. However, before tackling the role emotions play within the decision-making process, it is important to find which brain areas (or systems) are involved in emotions and what their respective roles are.

Note that for archival purposes, this project has been uploaded to [the University of Sheffield's Open Research Data Archive](https://figshare.shef.ac.uk/articles/software/The_Role_of_Emotions_in_Autonomous_Social_Agents_Python_Code_experiment_1_/14511117). Since this project might evolve in the future, it is advised to use the source code from the University of Sheffield's archive for further research.

## ProtoEmo
It was first theorized by Darwin in his [book](http://scholar.google.com/scholar?hl=en&btnG=Search&q=intitle:The+expression+of+emotions+in+man+and+animal#5), entitled: "The Expression of the Emotions in Man and Animals", that emotions are a mechanism which evolved to enhance the survival potential of animals. This idea has been passed down throughout the history of the field of affective science. Recently, it has inspired the work of [LeDoux](http://www.cell.com/article/S0896627312001298/fulltext), who suggested the existence of neural circuits at the base of the brain whose function is to ensure the survival of the individual. Hence, the name **Survival Circuits** attributed by LeDoux to this theory. Empirical results from conditioning experiments have led to the conclusion that the amygdala and thalamus are required for the acquisition and expression of a conditioned behavior. It is nowadays accepted that the thalamus is the entry way to the brain. Any information from the senses (except for the olfactory system) has to go through the thalamus before being dispatched to the brain systems in charge of processing this type of data. The amygdala has long been implicated in theories and models of emotions, but usually its role is simply described as *"processing emotions"*. Finally, although less explored than the other two areas, the hypothalamus is usually considered to be the control center for the autonomic nervous system. At an abstract level, the function fulfilled by the hypothalamus can be simplified to *"maintaining the body's homeostasis"*.

Given that the overall goal of my thesis is to investigate the role of emotions in the decision-making process, a logical first step would be to explore the applicability of LeDoux's survival circuit theory to groups of virtual agents. Consequently, the hypotheses this experiment seeks to verify are:
* **H1**: The circuit, made of the thalamus, the hypothalamus and the amygdala, and identified as essential to animal's survival, can also be used by virtual agents to influence their actions and increase their own survival capabilities.
* **H2**: The amygdala acts as the trigger for this survival circuit.
* **H3**: This circuit, however, is not enough to elicit any emotions, where emotions are differentiated from other mental states by their level of *'arousal'* and *'valence'*.

To explore the validity and consequences of the hypotheses formulated above, the [**ProtoEmo** architecture](http://aisb2017.cs.bath.ac.uk/conference-edition-proceedings.pdf) has been built. It is a structure mode of three main neural populations, each inspired by one of the brain areas mentioned so far: the thalamus, the hypothalamus and the amygdala.

## Resource foraging task
This project improves on the **one resource foraging** experiment implemented by [Scheutz](https://citeseerx.ist.psu.edu/viewdoc/summary?doi=10.1.1.94.5393). For an agent the goal of a resource foraging task is to explore the environment and forage for energy. In this case the environment is an infinite 2D surface with a pre-determined area centered around the origin. In this environment groups of robots, controlled either by [Scheutz's schema](https://citeseerx.ist.psu.edu/viewdoc/summary?doi=10.1.1.94.5393) or by the [**ProtoEmo** architecture](http://aisb2017.cs.bath.ac.uk/conference-edition-proceedings.pdf) have to learn the best strategy to improve their survival potential and, hopefully, procreate to increase the size of the population. The only actions available to each agent, to build its strategy, are: move, fight, flee, forage and procreate. Each action costs energy and processing the sensory data, describing the state of the environment, adds an additional unit of energy to the total amount consumed to perform said action.

The original implementation did not have any obstacles and used points to represent both agents and energy sources. This project was first intended to run using groups of [E-Puck](http://www.e-puck.org/) robots. Consequently, the dimensions for the agents, the obstacles and energy sources correspond to the real size of the e-puck robot, as well as other cylindrical objects that would have been used as part of the environment had the experiment been done in real life.

# Installation
## Requirements
This project has been implemented using [Python 3.7.6](https://www.python.org/downloads/release/python-376/) and requires the following packages:
```
bidict (0.19.0)
boto3 (1.11.5)
deap (1.3.0)
h5py (2.10.0)
Keras (2.3.1)
matplotlib (3.1.2)
numpy (1.18.4)
pandas (1.0.0)
scipy (1.4.1)
seaborn (0.10.0)
statsmodels (0.11.0)
tables (3.6.1)
tensorflow (1.15.0) or tensorflow-gpu (1.15.0), depending of the available hardware
ujson (1.35)
urllib3 (1.25.7)
```
For a more detailed list of all the required packages along with their dependencies please have a look at the [requirements.txt](requirements.txt) file.

## Installing using PIP
After installing `Python` and its associated package manager `pip`, one can easily install the rest of the requirements by simply issuing the following command:
``` bash
> pip install -U -r requirements.txt
```
or 
``` bash
> pip install --user -U -r requirements.txt
```

## Learning
During the learning phase, agents controlled by the **ProtoEmo** architecture learn via a Genetic Algorithm. Learning is done by executing the `init.py` script via the command line:
```bash
> python init.py -f ProtoEmoTf
```
An additional `--aws` parameter can be passed to the `init.py` script to indicate that both the data gathered during the learning phase and the checkpoint file should be uploaded to an Amazon Web Service (AWS) storage instance. See `python init.py --help` for more details on all the available parameters.
The number of generations over which to run the Genetic Algorithm can be configured in the [settings.py](settings.py) file by modifying the `MAX_GENERATION` parameter.
Upon first executing this command, the script will randomly initialize a population of individuals, then begin the learning process. After each generation a checkpoint file is created. Subsequent execution of the `init.py` script will look for a checkpoint file and resume the learning process.
Note: If you intend to use AWS for uploading the checkpoint file and the results to an S3 storage solution, you should change the `aws_access_key_id` and `aws_secret_access_key` in the [Genetic.py](Genetic.py) script on line 352-353.

## Testing and Generating the graphs containing the results
The testing phase can only be executed on the **master_test** branch of this project, since it requires heavily modifying the `Genetic.py` script.
The same is true for generating the different displays based on the results of the testing phase.
