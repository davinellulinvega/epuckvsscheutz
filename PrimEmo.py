#!/usr/bin/python
# -*- coding: utf-8 -*-
from math import hypot
import os
from thespian.actors import ActorExitRequest
from Robot import Robot
from settings import *
from Cylinder import Cylinder
from settings import *
from utils import extract_msg, format_msg
from EmergentInterface import IEmergent
from bidict import bidict


class PrimEmo(Robot):
    """
    A special implementation of the robot instance using the PrimEmo architecture as control mechanism.
    """

    def __init__(self):
        """
        Override the parent's constructor.
        Initialize and declare all the attributes necessary for a robot to sense its environment and maintain its
        homeostasis.
        """

        # Initialize the parent class
        super(PrimEmo, self).__init__()

        # Initialize the data logger
        self._data_log = None

        # Initialize the interface to emergent
        self._brain = None

        # Initialize temporary storage for the features
        self._sources = None
        self._rbts_pos = None
        self._rbts_act_tend = None
        self._obstacles = None

    def receiveMsg_dict(self, message, sender):
        """
        Override the parent method for receiving messages formatted as dictionaries.
        :param message: A dictionary containing the action to perform and any relevant data.
        :param sender: The ActorAddress of the sending unit.
        :return: Nothing.
        """

        # Extract the action and related data
        action, data = extract_msg(message)

        # Perform some complementary steps for the 'init' phase
        if action == 'update' and not self._dead:
            # Extract the environment's state from the data received
            self._sources = data.get('sources', dict())
            self._obstacles = data.get('obstacles', dict())
            robots_position = data.get('robots_pos', bidict())
            robots_act_tendency = data.get('robots_act', dict())

            # Remove the current Robot instance from the mapping between positions and robots
            self._rbts_pos, self._rbts_act_tend = self._remove_self(robots_position, robots_act_tendency)
            formatted_data = self._format_inputs(self._sources, self._rbts_pos, self._rbts_act_tend, self._obstacles)

            # Activate the brain
            self.send(self._brain, format_msg('activate', data={'formatted_data': formatted_data}))

        elif action == 'results' and not self._dead:
            # Get the outputs values
            outputs = data.get('outputs')

            # Update the value of the tendencies
            self._tendencies['action']['current'] = outputs[0]
            self._tendencies['fight'] = 100 * outputs[1] - 50
            self._tendencies['avoid'] = DEFAULT_AVOID_TENDENCY * outputs[2]  # To be on par with Scheutz, if avoid_tendency in [0, 1]
            self._tendencies['forage'] = DEFAULT_FORAGE_TENDENCY * outputs[3]  # To be on par with Scheutz, if forage_tendency in [0, 1]

            # Perform next action
            super(PrimEmo, self)._next_action(self._sources, self._rbts_pos, self._rbts_act_tend, self._obstacles)

            # Send the robot's new state to the supervisor
            self.send(self._supervisor, format_msg('update',
                                                   data={'id': self._id, 'position': self.get_position(),
                                                         'action_tendency': self._tendencies['action']['current']}))
        else:
            if action == 'init':
                # Get the brain address
                self._brain = data.get('load_balance')

                # Extract the simulation number and id from the data
                nb_sim = data.get('sim', 0)
                identifier = data.get('id', 0)

                self._data_log = open(os.path.join(BASE_DIR, "logs", "RobotData",
                                                   "sim_{}_{}_{}.log".format(nb_sim, __name__, identifier)), "a")

            # Forward both message and sender to the parent class
            super(PrimEmo, self).receiveMsg_dict(message, sender)

    def receiveMsg_ActorExitRequest(self, message, sender):
        """
        Define the process followed by the actor to gracefully shutdown.
        :param message: An instance of the ActorExitRequest class.
        :param sender: The ActorAddress of the sending unit, most likely the ActorSystem or the actor itself. Ignore it.
        :return: Nothing.
        """

        # Close the brain
        self._brain.close()

        # Forward both message and sender to parent class
        super(PrimEmo, self).receiveMsg_ActorExitRequest(message, sender)

    def _format_inputs(self, sources, robots_position, robots_act_tendency, obstacles):
        """
        Extract the necessary information from the environment and format it so that it can be sent to the brain for
        further processing.
        :param sources: Dict. The mapping between positions and sources.
        :param robots_position: Bidict. The mapping between positions and robots.
        :param robots_act_tendency: Dict. The mapping between action tendencies and robots.
        :param obstacles: Dict. The mapping between positions and obstacles.
        :return: Dictionary. A dictionary mapping the inputs required by the brain, to their respective value.
        """

        # Compute the distance between the current and environmental features
        obstacle_distance = self._get_dist(self._find_nearest_obstacle(obstacles))
        robot_distance = self._get_dist(self._find_nearest_robot(robots_position))
        source_distance = self._get_dist(self._find_nearest_source(sources))

        # Extract the maximum action tendency from all nearby robot
        max_act_tendency = self._get_max_action_tendency(robots_position, robots_act_tendency)

        # Compute the value corresponding to the amount of danger the robot should feel
        danger = (2 * OBSTACLE_THRES - min(2. * OBSTACLE_THRES, obstacle_distance)) * \
                 (3 * FIGHT_THRES - min(3. * FIGHT_THRES, robot_distance))

        # Initialize the dictionary of formatted data
        formatted_data = {
            'distances': {
                'source': source_distance / ROBOT_HORIZON,
                'robot': robot_distance / ROBOT_HORIZON,
                'obstacle': obstacle_distance / ROBOT_HORIZON
            },
            'energy': self._energy / PROCREATE_ENERGY_REQUIRED,  # Can get over 1, but not too far over
            'max_action_tendency': max_act_tendency,
            'curr_action_tendency': self._tendencies['action']['current'],
            'danger': danger,
            'tendencies': {
                'forage': self._compute_forage_tendency(source_distance, robot_distance, danger, max_act_tendency),
                'avoid': self._compute_avoid_tendency(obstacle_distance, danger),
                'fight': self._compute_fight_tendency(robot_distance, max_act_tendency, danger),
                'action': self._compute_action_tendency(danger)
              }
        }

        # Return the dictionary of formatted data
        return formatted_data

    def _compute_forage_tendency(self, dist_src, dist_rbt, danger, max_act_tendency):
        """
        Compute the value of the forage tendency given the current state of the environment.
        :param dist_src: A float representing the distance from the robot to the nearest resource.
        :param dist_rbt: A float representing the distance from the robot to the nearest teammate.
        :param danger: A float representing the amount of danger the robot is in.
        :param max_act_tendency: A float representing the maximum value of the action tendencies of all nearby robots.
        :return: Float. The value of the forage tendency in the given situation.
        """

        # Compute the contribution from the energy input
        if self._energy <= CRITICAL_ENERGY_LVL:
            energy_contrib = 1
        elif self._energy > PROCREATE_ENERGY_REQUIRED:
            energy_contrib = 0.05
        else:
            energy_contrib = (0.95 * self._energy + 0.05 * CRITICAL_ENERGY_LVL - PROCREATE_ENERGY_REQUIRED) / \
                             (CRITICAL_ENERGY_LVL - PROCREATE_ENERGY_REQUIRED)  # Linear equation

        # Compute the contribution from maximum action tendency
        if max_act_tendency > self._tendencies['action']['current']:
            act_tendency_contrib = dist_rbt / ROBOT_HORIZON  # If someone else nearby needs it more, let it have it
        else:
            act_tendency_contrib = 1  # Else get the energy

        # Compute the contribution the distance from robot to source has (makes the robot opportunistic)
        dist_contrib = 1 - (dist_src / (2 * ROBOT_HORIZON))

        # Return the value of the forage tendency
        return energy_contrib * (1 - danger) * act_tendency_contrib * dist_contrib

    def _compute_avoid_tendency(self, dist_obs, danger):
        """
        Compute the value of the avoid tendency given the current environment and robot's state.
        :param dist_obs: A float representing the distance from the robot to the nearest obstacle.
        :param danger: A float value representing the amount of danger the robot is in.
        :return: Float. The value of the avoid tendency.
        """

        # Compute the contribution of the energy input
        energy_contrib = min(self._energy, PROCREATE_ENERGY_REQUIRED) / PROCREATE_ENERGY_REQUIRED

        # Compute the contribution of the distance from robot to nearest obstacle
        dist_contrib = 1 - (dist_obs / ROBOT_HORIZON)

        # Compute the contribution of the danger
        danger_contrib = 0.95 * danger + 0.05

        # Return the avoid tendency
        return energy_contrib * dist_contrib * danger_contrib

    def _compute_fight_tendency(self, dist_rbt, max_act_tend, danger):
        """
        Compute the value of the fight tendency given the current state of the environment and the robot's state.
        :param dist_rbt: A float representing the distance between the robot and its nearest teammate.
        :param max_act_tend: A float representing the maximum value of the action tendencies from the nearby robots.
        :param danger: A float representing the amount of danger the current robot is in.
        :return: Float. The value of the fight tendency.
        """

        # Compute the contribution of the max action tendency
        if self._tendencies['action']['current'] > max_act_tend:
            act_tend_contrib = 1
        else:
            act_tend_contrib = 0.5

        # Compute the contribution of the energy
        energy_contrib = min(self._energy, PROCREATE_ENERGY_REQUIRED) / PROCREATE_ENERGY_REQUIRED

        # Compute the contribution of the distance to the nearest robot
        dist_contrib = 1 - (dist_rbt / (2 * ROBOT_HORIZON))

        # Return the fight tendency
        return act_tend_contrib * (1 - danger) * energy_contrib * dist_contrib

    def _compute_action_tendency(self, danger):
        """
        Compute the value of the action tendency given the current state of the environment and robot's state.
        :param danger: A float representing the amount of danger the robot is in.
        :return: Float. The value of the action tendency.
        """

        # Compute the contribution of the energy level
        if self._energy <= CRITICAL_ENERGY_LVL:
            energy_contrib = 1
        elif self._energy >= PROCREATE_ENERGY_REQUIRED:
            energy_contrib = 0
        else:
            energy_contrib = (-self._energy + PROCREATE_ENERGY_REQUIRED) / (PROCREATE_ENERGY_REQUIRED - CRITICAL_ENERGY_LVL)

        # Return the action tendency
        return (energy_contrib + danger) / 2

    def _get_dist(self, cylinder):
        """
        Get the distance to a given cylinder in the robot's frame of reference.
        :param cylinder: An instance of the cylinder class.
        :return: Float. The distance in question.
        :rtype: Float
        """

        # If the object given in parameter is not an instance of the Cylinder class
        if not isinstance(cylinder, Cylinder):
            return 0

        # Get the cylinder's position in the world
        pos_x, pos_y = cylinder.get_position()

        # Transform the position into the robot's frame
        pos_x -= self._pos_x
        pos_y -= self._pos_y

        # Compute and return the distance
        return hypot(pos_x, pos_y)

    def _get_max_action_tendency(self, robots_position, robots_act_tendency):
        """
        Get the maximal value of the action tendency for all robots within the horizon.
        :param robots_position: Bidict. A dictionary mapping positions and robots.
        :param robots_act_tendency: Dict. A dictionary mapping action tendencies and robots.
        :return: Float. 0 if no robots in range. Else the maximum value of the action tendencies.
        """

        # Get the list of all nearby robots
        nearby_rbt = self._find_nearest_robot(robots_position, ROBOT_HORIZON)

        # Check if there are any robots within the horizon
        if len(nearby_rbt) == 0:
            # If no one out there return 0
            return 0

        # Get the action tendencies of all the robots
        rbt_tendencies = [robots_act_tendency[rbt_idx] for rbt_idx in nearby_rbt]

        # Return the maximum value
        return max(rbt_tendencies)
