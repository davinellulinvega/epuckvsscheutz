#!/usr/bin/python
# -*- coding: utf-8 -*-
import os
import random
import multiprocessing
import pickle
from functools import partial
import logging
import boto3
from botocore.exceptions import ClientError
import tensorflow as tf
import numpy
from deap import base, creator, tools, algorithms
import settings
from ProtoEmoTf import ProtoEmoTf
from Supervisor import Supervisor


LOGGER = logging.getLogger('')


def build_ann_model(individuals):
    """
    Build the ProtoEmo architecture using the TensorFlow framework.
    :param individuals: A list of matrices, each an individual representing a specie.
    :return: A tuple containing the external input, internal input, recurrent CA input, CA layer, output layer.
    """

    # Reset the default graph to remove all created nodes and therefore free up memory
    tf.reset_default_graph()

    # Declare some placeholders for the inputs and the recurrent connection from the central amygdala layer
    ext_in = tf.placeholder(dtype=tf.float32, shape=(settings.EXT_IN_LAYER_SIZE,), name="external_in")
    int_in= tf.placeholder(dtype=tf.float32, shape=(settings.INT_IN_LAYER_SIZE,), name="internal_in")
    rec_ca_in = tf.placeholder(dtype=tf.float32, shape=(settings.CA_LAYER_SIZE,), name="ca_out")

    # Define the thalamus
    ext_in_and_ca_out = tf.concat([tf.negative(rec_ca_in, name="inhib_ca"), ext_in], axis=0)
    w_in_and_ca_to_thal = tf.constant(individuals[0], dtype=tf.float32,
                                      shape=(settings.THAL_LAYER_SIZE, settings.EXT_IN_LAYER_SIZE +
                                             settings.CA_LAYER_SIZE))
    thalamus = tf.nn.leaky_relu(tf.tensordot(w_in_and_ca_to_thal, ext_in_and_ca_out, axes=1), name="thalamus")
    thalamus = tf.nn.l2_normalize(thalamus, dim=0)

    # Define the hypothalamic layers
    int_in_and_ca_out = tf.concat([tf.negative(rec_ca_in, name="inhib_ca"), int_in], axis=0)
    w_in_to_lah = tf.constant(individuals[1], dtype=tf.float32,
                              shape=(settings.LAT_HYPO_LAYER_SIZE, settings.INT_IN_LAYER_SIZE +
                                     settings.CA_LAYER_SIZE))
    lat_hypo = tf.nn.leaky_relu(tf.tensordot(w_in_to_lah, int_in_and_ca_out, axes=1), name="lateral_hypothalamus")
    lat_hypo = tf.nn.l2_normalize(lat_hypo, dim=0)

    w_lah_to_hypo = tf.constant(individuals[2], dtype=tf.float32,
                                shape=(settings.HYPO_LAYER_SIZE, settings.LAT_HYPO_LAYER_SIZE))
    hypo = tf.nn.leaky_relu(tf.tensordot(w_lah_to_hypo, lat_hypo, axes=1), name="hypothalamus")
    hypo = tf.nn.l2_normalize(hypo, dim=0)

    # Define the amygdalar layers
    w_thal_to_la = tf.constant(individuals[3], dtype=tf.float32,
                               shape=(settings.LA_LAYER_SIZE, settings.THAL_LAYER_SIZE))
    lateral_amyg = tf.nn.leaky_relu(tf.tensordot(w_thal_to_la, thalamus, axes=1), name="lateral_amygdala")
    lateral_amyg = tf.nn.l2_normalize(lateral_amyg, dim=0)

    w_la_to_ba = tf.constant(individuals[4], dtype=tf.float32,
                             shape=(settings.BA_LAYER_SIZE, settings.LA_LAYER_SIZE))
    basal_amyg = tf.nn.leaky_relu(tf.tensordot(w_la_to_ba, lateral_amyg, axes=1), name="basal_amygdala")
    basal_amyg = tf.nn.l2_normalize(basal_amyg, dim=0)

    ba_and_la = tf.concat([basal_amyg, lateral_amyg], axis=0)
    w_la_and_ba_to_itc = tf.constant(individuals[5], dtype=tf.float32,
                                     shape=(settings.ITC_LAYER_SIZE, settings.LA_LAYER_SIZE +
                                            settings.BA_LAYER_SIZE))
    itc_amyg = tf.nn.leaky_relu(tf.tensordot(w_la_and_ba_to_itc, ba_and_la, axes=1), name="itc_amygdala")
    itc_amyg = tf.nn.l2_normalize(itc_amyg, dim=0)
    itc_amyg = tf.negative(itc_amyg)

    hypo_itc_and_ba = tf.concat([hypo, itc_amyg, basal_amyg], axis=0)
    w_hypo_itc_and_ba_to_ca = tf.constant(individuals[6], dtype=tf.float32, shape=(settings.CA_LAYER_SIZE,
                                                                                   settings.HYPO_LAYER_SIZE +
                                                                                   settings.BA_LAYER_SIZE +
                                                                                   settings.ITC_LAYER_SIZE))
    central_amyg = tf.nn.leaky_relu(tf.tensordot(w_hypo_itc_and_ba_to_ca, hypo_itc_and_ba, axes=1),
                                    name="central_amygdala")
    central_amyg = tf.nn.l2_normalize(central_amyg, dim=0)

    # Finally define the output layer
    w_ca_to_out = tf.constant(individuals[7], dtype=tf.float32, shape=(settings.OUT_LAYER_SIZE,
                                                                       settings.CA_LAYER_SIZE))
    output = tf.nn.softmax(tf.tensordot(w_ca_to_out, central_amyg, axes=1), name="output")
    # output = tf.nn.l2_normalize(output, dim=0)

    # Return the output and central amygdala tensors
    return ext_in, int_in, rec_ca_in, central_amyg, output


def upload_logbook(aws_clt):
    """
    Upload the logbook separately, so that one may work on it and extract useful statistics.
    :param aws_clt: An instance of the S3 client connected to AWS.
    :return: Nothing.
    """

    # Leave a warning for the user
    LOGGER.warning("Uploading logbook to S3 bucket.")

    # Upload the checkpoint files if possible
    if aws_clt is not None:
        aws_clt.upload_file(settings.GA_LOGBOOK_FILENAME, 'epuckvsscheutz',
                            os.path.basename(settings.GA_LOGBOOK_FILENAME))  # The last parameter is the key


def save_logbook(logbook):
    """
    Save the logbook separately, so that one may work on it and extract useful statistics.
    :param logbook: An instance of the Logbook class.
    :return: Nothing.
    """

    # Leave a warning for the user
    LOGGER.warning("Saving logbook to file: {}".format(settings.GA_LOGBOOK_FILENAME))

    # Open the pickled file and dump the content of the logbook in it
    with open(settings.GA_LOGBOOK_FILENAME, "wb") as log_file:
        pickle.dump(logbook, log_file)


def upload_checkpoint(aws_clt):
    """
    Connect to the S3 bucket and upload the checkpoint file. A precaution against sudden termination.
    :param aws_clt: An instance of the S3 client connected to AWS.
    :return: Nothing.
    """

    # Leave a warning for the user
    LOGGER.warning("Uploading checkpoint to S3 bucket.")

    # Upload the checkpoint files if possible
    if aws_clt is not None:
        aws_clt.upload_file(settings.GA_CHECKPOINT_FILENAME, 'epuckvsscheutz',
                            os.path.basename(settings.GA_CHECKPOINT_FILENAME))  # The last parameter is the key


def download_checkpoint(aws_clt):
    """
    Connect to the S3 bucket and download the checkpoint file, if available.
    :param aws_clt: An instance of the S3 client connected to AWS.
    :return: Nothing.
    """

    # Leave a warning for the user
    LOGGER.warning("Trying to download the checkpoint from the S3 bucket.")

    try:
        # Try to fetch the checkpoint file
        aws_clt.download_file('epuckvsscheutz', os.path.basename(settings.GA_CHECKPOINT_FILENAME),
                              settings.GA_CHECKPOINT_FILENAME)
    except ClientError:
        LOGGER.error("Error while trying to download the file {}: "
                     "No such file or directory.".format(settings.GA_CHECKPOINT_FILENAME))


def save_checkpoint(species, specie_start, generation, representatives, next_representatives, logbook, rnd_state, halls_of_fame):
    """
    Saves the state of the genetic algorithm to a file, so it can be resumed later on.
    :param species: List of lists representing the current population of every species.
    :param specie_start: An integer indicating the index at which to start processing the species.
    :param generation: An integer identifying the current generation.
    :param representatives: List of lists representing the best individual of each species.
    :param next_representatives: List of list representing the best individual of the offspring from each specie.
    :param logbook: An instance of the Logbook class, used to keep a tab on every individual.
    :param rnd_state: The current state of the Random module, obtained from Random.getstate
    :return: Nothing.
    """

    # Leave a warning for the user
    LOGGER.warning("Saving checkpoint to file: {}".format(settings.GA_CHECKPOINT_FILENAME))

    # Build the dictionary containing the new checkpoint
    checkpoint = dict(species=species, specie_start=specie_start, generation=generation,
                      representatives=representatives, next_representatives=next_representatives, logbook=logbook,
                      rnd_state=rnd_state, halls_of_fame=halls_of_fame)

    # A record the checkpoint in its dedicated file
    with open(settings.GA_CHECKPOINT_FILENAME, "wb") as checkpoint_file:
        pickle.dump(checkpoint, checkpoint_file)


def evaluate(individual, generation, specie_idx, individual_idx):
    """
    Evaluate the fitness of a given individual.
    :param individual: A list of matrices, each an individual of a given specie.
    :param generation: An integer representing the current generation.
    :param specie_idx: An integer representing the index of the current species.
    :param individual_idx: An integer representing the index of the current individual.
    :return: A tuple representing the individual's fitness for each objectives.
    """

    # Build the artificial neural network
    ext_in, int_in, rec_ca_in, ca_layer, out_layer = build_ann_model(individual)

    # Only reserve half of the memory GPU memory available to allow other processes to use the GPU
    # config = tf.ConfigProto()
    # config.gpu_options.per_process_gpu_memory_fraction = settings.GPU_MEMORY_RATIO

    # Open a new tensorflow session
    # session = tf.Session(config=config)
    session = tf.Session()

    # Partially initialize the robot's factory
    rbt_factory = partial(ProtoEmoTf, ext_in=ext_in, int_in=int_in, rec_ca_in=rec_ca_in, ca_layer=ca_layer,
                          out_layer=out_layer, session=session, gen=generation, spec_idx=specie_idx,
                          ind_idx=individual_idx)
    # Initialize the Supervisor
    supervisor = Supervisor(rbt_factory, 0)
    # Run the supervisor, which will gather most of the results for us
    stats = supervisor.run()

    # Delete the supervisor
    del supervisor

    # Close the tensorflow session and free all resources
    session.close()
    del session

    # Return the two measures of fitness
    return stats['population'], stats['max_population']


def mutate_gaussian(individual, mu, sigma, indpb):
    """
    Cycle through all the rows of a given individual and apply random gaussian mutation to each element of a row.
    :param individual: A matrix representing an individual.
    :param mu: A float representing the mean of the gaussian distribution.
    :param sigma: A float representing the standard deviation of the gaussian distribution.
    :param indpb: A float representing the independent mutation probability.
    :return: A tuple, containing one individual.
    """

    # Cycle through the individual's rows
    for i, row in enumerate(individual):
        # Apply the standard gaussian mutation
        mutant, = tools.mutGaussian(row, mu=mu, sigma=sigma, indpb=indpb)
        # Update the individual with the mutant row
        individual[i] = mutant

    # Return the mutated matrix
    return individual,


def two_pts_crossover(ind1, ind2):
    """
    Perform a crossover of two individuals represented by matrices. The crossover is performed row-wise.
    :param ind1: A matrix representing the first parent.
    :param ind2: A matrix representing the second parent.
    :return: A tuple, containing two children.
    """

    # Cycle through the individuals' rows
    for idx in range(len(ind1)):
        # Get the two parents
        p1, p2 = ind1[idx], ind2[idx]
        # Mate
        c1, c2 = tools.cxTwoPoint(p1, p2)
        # Put back into respective individual
        ind1[idx], ind2[idx] = c1, c2

    # Return the crossed individuals
    return ind1, ind2


# Declare and define the building block of the genetic algorithm
creator.create("FitnessMax", base.Fitness, weights=(1.0, 0.5))  # remaining population, maximum population
creator.create("Individual", list, fitness=creator.FitnessMax)

toolbox = base.Toolbox()
toolbox.register("rnd_val", random.gauss, 0.5, 0.5)

# Ext_in + Ca_out to Thal
toolbox.register("ect_row", tools.initRepeat, list, toolbox.rnd_val, n=(settings.EXT_IN_LAYER_SIZE +
                                                                        settings.CA_LAYER_SIZE))
toolbox.register("ect_ind", tools.initRepeat, creator.Individual, toolbox.ect_row, n=settings.THAL_LAYER_SIZE)
toolbox.register("ect_specie", tools.initRepeat, list, toolbox.ect_ind, n=settings.POPULATION_SIZE)

# Int_in + Ca_out to Lat_hypo
toolbox.register("icl_row", tools.initRepeat, list, toolbox.rnd_val, n=(settings.INT_IN_LAYER_SIZE +
                                                                        settings.CA_LAYER_SIZE))
toolbox.register("icl_ind", tools.initRepeat, creator.Individual, toolbox.icl_row, n=settings.LAT_HYPO_LAYER_SIZE)
toolbox.register("icl_specie", tools.initRepeat, list, toolbox.icl_ind, n=settings.POPULATION_SIZE)

# Lat_hypo to Hypo
toolbox.register("lh_row", tools.initRepeat, list, toolbox.rnd_val, n=settings.LAT_HYPO_LAYER_SIZE)
toolbox.register("lh_ind", tools.initRepeat, creator.Individual, toolbox.lh_row, n=settings.HYPO_LAYER_SIZE)
toolbox.register("lh_specie", tools.initRepeat, list, toolbox.lh_ind, n=settings.POPULATION_SIZE)

# Thal to La
toolbox.register("tl_row", tools.initRepeat, list, toolbox.rnd_val, n=settings.THAL_LAYER_SIZE)
toolbox.register("tl_ind", tools.initRepeat, creator.Individual, toolbox.tl_row, n=settings.LA_LAYER_SIZE)
toolbox.register("tl_specie", tools.initRepeat, list, toolbox.tl_ind, n=settings.POPULATION_SIZE)

# La to Ba
toolbox.register("lb_row", tools.initRepeat, list, toolbox.rnd_val, n=settings.LA_LAYER_SIZE)
toolbox.register("lb_ind", tools.initRepeat, creator.Individual, toolbox.lb_row, n=settings.BA_LAYER_SIZE)
toolbox.register("lb_specie", tools.initRepeat, list, toolbox.lb_ind, n=settings.POPULATION_SIZE)

# La + Ba to Itc
toolbox.register("lbi_row", tools.initRepeat, list, toolbox.rnd_val, n=(settings.LA_LAYER_SIZE +
                                                                        settings.BA_LAYER_SIZE))
toolbox.register("lbi_ind", tools.initRepeat, creator.Individual, toolbox.lbi_row, n=settings.ITC_LAYER_SIZE)
toolbox.register("lbi_specie", tools.initRepeat, list, toolbox.lbi_ind, n=settings.POPULATION_SIZE)

# Hypo + itc + Ba to Ca
toolbox.register("hibc_row", tools.initRepeat, list, toolbox.rnd_val, n=(settings.BA_LAYER_SIZE +
                                                                         settings.HYPO_LAYER_SIZE +
                                                                         settings.ITC_LAYER_SIZE))
toolbox.register("hibc_ind", tools.initRepeat, creator.Individual, toolbox.hibc_row, n=settings.CA_LAYER_SIZE)
toolbox.register("hibc_specie", tools.initRepeat, list, toolbox.hibc_ind, n=settings.POPULATION_SIZE)

# Ca to Out
toolbox.register("co_row", tools.initRepeat, list, toolbox.rnd_val, n=settings.CA_LAYER_SIZE)
toolbox.register("co_ind", tools.initRepeat, creator.Individual, toolbox.co_row, n=settings.OUT_LAYER_SIZE)
toolbox.register("co_specie", tools.initRepeat, list, toolbox.co_ind, n=settings.POPULATION_SIZE)

toolbox.register("mate", two_pts_crossover)
toolbox.register("mutate", mutate_gaussian, mu=0.5, sigma=0.25, indpb=0.01)  # mu and sigma are extracted from Leabra default parameters
toolbox.register("select", tools.selTournament, tournsize=3)
toolbox.register("get_best", tools.selBest, k=1)
toolbox.register("evaluate", evaluate)

# Take advantage of any multiprocessing capabilities
pool = multiprocessing.Pool()
toolbox.register("map", pool.map)

# Define the statistics that will be kept up to date for every species during execution
stats = tools.Statistics(lambda ind: ind.fitness.values)
# Defining the axis on which to compute the stats, prevents numpy from concatenating results for the first and second objectives
stats.register("avg", numpy.mean, axis=0)
stats.register("std", numpy.std, axis=0)
stats.register("min", numpy.min, axis=0)
stats.register("max", numpy.max, axis=0)


def main(on_aws=False):
    """
    The main function of the Genetic Algorithm module.
    Coevolves the individuals from every species to reach a satisfactory solution.
    :param on_aws: A boolean indicating if the script is executed on an AWS ec2 instance.
    :return: A list of matrices, containing the best representative for each specie.
    """

    # Initialize the Amazon Web Service client if necessary
    if on_aws and not os.path.isfile(settings.GA_CHECKPOINT_FILENAME):
        s3_client = boto3.client(service_name='s3', aws_access_key_id="AKIAI2WQRFSLOTBJ37EQ",
                                 aws_secret_access_key="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")
        # And download the checkpoint file if any available
        download_checkpoint(s3_client)

    # If a checkpoint file exist in the directory
    if os.path.isfile(settings.GA_CHECKPOINT_FILENAME):
        # Load the last known state of the genetic algorithm
        with open(settings.GA_CHECKPOINT_FILENAME, "rb") as checkpoint_file:
            checkpoint = pickle.load(checkpoint_file)

        # Assign the retrieved values to the variables
        species = checkpoint["species"]
        specie_start = checkpoint["specie_start"]
        gen = checkpoint["generation"]
        representatives = checkpoint["representatives"]
        next_repr = checkpoint['next_representatives']
        logbook = checkpoint["logbook"]
        random.setstate(checkpoint["rnd_state"])
        halls_of_fame = checkpoint.get('halls_of_fame', [tools.HallOfFame(maxsize=2) for _ in range(settings.SPECIES_SIZE)])

        print('continuing from generation {}, specie {}.'.format(gen, specie_start))

        # Leave a message to warn the user where the beginning will be
        LOGGER.warning("Continuing from generation {}, specie {}.".format(gen, specie_start))
    else:
        # Start everything anew
        species = [toolbox.ect_specie(), toolbox.icl_specie(), toolbox.lh_specie(), toolbox.tl_specie(),
                   toolbox.lb_specie(), toolbox.lbi_specie(), toolbox.hibc_specie(), toolbox.co_specie()]
        specie_start = 0
        representatives = [random.choice(s) for s in species]
        next_repr = [None] * len(species)
        logbook = tools.Logbook()
        logbook.header = "generation", "species", "evals", "avg", "std", "min", "max"
        halls_of_fame = [tools.HallOfFame(maxsize=2) for _ in range(settings.SPECIES_SIZE)]
        gen = 0

    # Loop until the algorithm reached the last generation
    while gen < settings.MAX_GENERATION:
        LOGGER.warning("Evaluating species of the {}th generation.".format(gen))
        # Loop over the species
        for specie_idx in range(specie_start, settings.SPECIES_SIZE):
            LOGGER.warning("Onward and forward with the {}th specie.".format(specie_idx+1))
            # Get the next specie
            s = species[specie_idx]
            # Crossover and mutate individuals within the specie
            s = algorithms.varAnd(s, toolbox, 0.6, 0.1)  # Last two params are: cxpb and mutpb
            # Get the left and right representatives of the other species
            r_left, r_right = representatives[0:specie_idx], representatives[specie_idx+1:]
            # Evaluate each individual of the current specie
            for ind_idx, ind in enumerate(s):
                # Concatenate the individual with the other representatives
                individual = r_left + [ind] + r_right
                # Get the fitness
                ind.fitness.values = toolbox.evaluate(individual, gen, specie_idx, ind_idx)
            # Gather statistics on the current specie
            record = stats.compile(s)
            logbook.record(generation=gen, species=specie_idx, evals=len(s), **record)
            # Save the logbook to disk
            save_logbook(logbook)

            # Select the individuals that stay in the current specie
            species[specie_idx] = toolbox.map(toolbox.clone, toolbox.select(s, len(s)))
            # Select the individual that will represent this specie from now on
            next_repr[specie_idx] = toolbox.get_best(s)[0]

            # Update the hall of fame for the current specie
            halls_of_fame[specie_idx].update(s)

            # Establish a checkpoint so as not to loose everything in case of termination
            save_checkpoint(species, specie_idx + 1, gen, representatives, next_repr,
                            logbook, random.getstate(), halls_of_fame)

            # If running on an AWS instance, upload the checkpoint and logbook files to a bucket for safety
            if on_aws:
                upload_checkpoint(s3_client)
                upload_logbook(s3_client)

        # Reinitialize the starting specie, since this is the end of the generation
        specie_start = 0

        # Increment the number of generations
        gen += 1

        # Update the list of representatives for each specie
        representatives = next_repr

        # Initialize the container for the next representatives
        next_repr = [None] * len(species)

        # Establish a checkpoint so as not to loose everything in case of termination
        save_checkpoint(species, specie_start, gen, representatives, next_repr,
                        logbook, random.getstate(), halls_of_fame)

        # If running on an AWS instance, upload the checkpoint and logbook files to a bucket for safety
        if on_aws:
            upload_checkpoint(s3_client)
