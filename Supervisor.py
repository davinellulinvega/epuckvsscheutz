#!/usr/bin/python
# -*- coding: utf-8 -*-
import random
import logging
import ujson
from World import World
import settings


class Supervisor:
    """
    Define a supervisor for the simulation, in charge of adding/removing sources and robots, as well as initializing the
    environment.
    """

    def __init__(self, rbt_factory, sim_number):
        """
        Define and initialize some basic attributes required to manage the simulation.
        """

        # Get the logger for the Supervisor
        self._logger = logging.getLogger("Supervisor")

        # Initialize the robot factory
        self._robot_factory = rbt_factory

        # Initialize a cycle meter for the overall simulation
        self._cycle = 0

        # Initialize the set of robots id which have been updated
        self._children = []

        # Initialize the simulation number
        self._sim = sim_number

        # Initialize the Supervisor with the given parameters
        self.world = None
        self._init(settings.ROBOT_POP_SIZE, settings.SOURCE_POP_SIZE, settings.OBSTACLE_POP_SIZE)

    def run(self):
        """
        The supervisor's main function. For each simulation cycle, calls the run method on each of its child, add energy
        sources to the environment
        :return: dict. A dictionary containing statistics concerning the robot's population and its performance.
        """

        # Initialize some useful variables
        cycle = 0
        total_years_lived_so_far = 0

        # Initialize a dictionary of statistics for the genetic algorithm
        stats = {'cycle': cycle, 'population': len(self._children), 'avg_life_span': 0,
                 'max_population': len(self._children)}

        # Run the simulation while at least one robot is alive and the max number of cycle has not been reached
        while cycle < settings.MAX_NB_CYCLES and len(self._children) > 0:
            # Gather and log statistics for GA fitness
            total_years_lived_so_far += len(self._children)
            stats['max_population'] = max(stats['max_population'], len(self._children))
            stats['avg_life_span'] = total_years_lived_so_far / stats['max_population']
            stats['population'] = len(self._children)
            stats['cycle'] = cycle
            self._logger.info(ujson.dumps(stats))

            # Call the run method on all children in random order, to avoid any advantage that might bring to the robot
            for robot in random.sample(self._children, len(self._children)):
                robot.run(cycle)

            # Create a new random energy source
            self.world.add_source()

            # Increase the cycle meter
            cycle += 1

        # Delete all linked objects
        for child in self._children:
            self.remove_robot(child)

        del self._children
        self.world.empty()
        del self.world

        # Return all gathered statistics
        return stats

    def add_robot(self):
        """
        Add a new robot to the environment, at a random location.
        :return: Nothing.
        """

        # Pre-initialize the new robot
        robot = self._robot_factory(id_rbt=len(self._children), supervisor=self)

        # Add the robot to the world and finalize its initialization
        robot = self.world.add_robot(robot)

        # Create a child actor
        self._children.append(robot)

    def remove_robot(self, robot):
        """
        Remove a robot from the list of children and the environment upon death.
        :param robot: An instance of the Robot class.
        :return: Nothing.
        """

        # Remove the robot from the list of children
        try:
            self._children.remove(robot)
        except ValueError:
            self._logger.exception("Error: Could not find robot: {}, within children: {}".format(robot, self._children))

        # Remove the robot from the environment
        self.world.remove_robot(robot)

        # Shut the robot down
        robot.shutdown()

        # Delete the instance from memory
        del robot

    def get_simulation_number(self):
        """
        Returns an integer representing the current simulation number.
        :return: Int.
        """

        return self._sim

    def _init(self, nb_robots, nb_sources, nb_obstacles):
        """
        Initialize a new world and all its sub-components: robots, sources and obstacles.
        :param nb_robots: Int. The initial number of robots to place on the map.
        :param nb_sources: Int. The initial number of sources to place on the map.
        :param nb_obstacles: Int. The initial number of obstacles to place on the map.
        :return: Nothing.
        """

        # Initialize the world
        self.world = World()

        # Add obstacles until reaching the right population size
        while self.world.get_obstacles_size() < nb_obstacles:
            self.world.add_obstacle()

        # Add sources to the world until the population reaches the right size
        while self.world.get_sources_size() < nb_sources:
            self.world.add_source()

        # Add robots to the world until the population reaches the right size
        while self.world.get_robots_size() < nb_robots:
            self.add_robot()
