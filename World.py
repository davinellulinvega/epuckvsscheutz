#!/usr/bin/python
# -*- coding: utf-8 -*-
import logging
import random
from bidict import bidict, ValueDuplicationError
import settings
from Cylinder import Cylinder
from Obstacle import Obstacle
from Source import Source


class World:
    """
    Defines a container for all the objects that you could find inside a simulation. Along with settings related to 
    environmental features.
    """

    def __init__(self):
        """
        Define and initialize all the containers and settings necessary to run the simulation.
        """

        self._logger = logging.getLogger("World")

        # Define container to store the different objects part of the simulation
        self._robots_position = bidict()  # Contains bidirectional mapping {robot: position, ...}
        self._robots_action_tendency = dict()  # Contains mapping {robot: action_tendency, ...} and return 0 by default
        self._obstacles = dict()  # Obstacles are defined as a dict mapping position to obstacle directly since they are static
        self._sources = dict()

    def get_position_source_map(self):
        """
        A getter for the mapping between positions and sources.
        :return: Dict. The mapping between positions and sources.
        """

        return self._sources

    def get_position_robot_map(self):
        """
        A getter for the mapping between positions and robots.
        :return: Bidict. The bidirectional mapping between positions and robots' id.
        """

        return self._robots_position

    def get_act_tendency_robot_map(self):
        """
        A getter for the mapping between action tendency and robots.
        :return: Dict. A dictionary mapping robot ids with their action tendency.
        """

        return self._robots_action_tendency

    def get_position_obstacle_map(self):
        """
        A getter for the mapping between positions and obstacles.
        :return: Dict. The mapping between positions and obstacles.
        """

        return self._obstacles

    def update_position_robot_map(self, rbt, new_pos):
        """
        Update the mapping between robot id and the its current position.
        :param rbt: An instance of the Robot class.
        :param new_pos: A tuple representing the robot's current position along the X and Y axis.
        :return: Nothing.
        """

        # Store the mapping between the position and the robot
        if new_pos is not None:
            self._robots_position[rbt] = new_pos

    def update_action_tendency_robot_map(self, rbt, act_tend):
        """
        Update the mapping between a given robot and its action tendency.
        :param rbt: An instance of the Robot class.
        :param act_tend: A value representing the robot's current action tendency.
        :return: Nothing.
        """

        # Store the mapping between id and action tendency
        self._robots_action_tendency[rbt] = act_tend

    def get_obstacles_size(self):
        """
        Retrieve the size of the obstacle population.
        :return: Int. The size of the population.
        """

        return len(self._obstacles)

    def get_sources_size(self):
        """
        Retrieve the size of the source population.
        :return: Int. The size of the population.
        """

        return len(self._sources)

    def get_robots_size(self):
        """
        Retrieve the size of the robot population.
        :return: Int. The size of the population.
        """

        return len(self._robots_position)

    def add_obstacle(self):
        """
        Add a simple obstacle to the environment, if it does not collide with any other obstacle
        :return: Boolean. True if the obstacle was added correctly. False otherwise.
        """

        collide = True
        while collide:
            collide = False
            obstacle = Obstacle(len(self._obstacles), pos_x=random.uniform(settings.MIN_X, settings.MAX_X),
                                pos_y=random.uniform(settings.MIN_Y, settings.MAX_Y))

            # Check if the obstacle collides with any other obstacle
            for o_obs in self._obstacles.values():
                if o_obs.collide(obstacle.get_position(), radius=settings.OBJECT_RADIUS):
                    # Collision
                    collide = True
                    break

            if not collide:
                # Check if the obstacle collides with any robot
                for robot_pos in self._robots_position.values():
                    if obstacle.collide(robot_pos, radius=settings.ROBOT_BODY_RADIUS):
                        # Collision
                        collide = True
                        break

        # Add the obstacle to the environment
        self._obstacles.update({obstacle.get_position(): obstacle})

        # All went well
        return True

    def remove_obstacle(self, position):
        """
        Remove the obstacle found at the given position.
        :param position: The obstacle's position along the X and Y axis.
        :return: Nothing.
        """

        # Remove the obstacle from the world
        obstacle = self._obstacles.pop(position)

        # Delete the obstacle from memory
        del obstacle

    def add_source(self):
        """
        Add a given source to the environment if it does not collide with any obstacle.
        :param source: An instance of the Source class.
        :return: Boolean. True if the source was added to the environment. False otherwise.
        """

        collide = True
        while collide:
            collide = False
            source = Source(len(self._sources), pos_x=random.uniform(settings.MIN_X, settings.MAX_X),
                            pos_y=random.uniform(settings.MIN_Y, settings.MAX_Y))
            # Check if the source collides with any obstacle
            for obstacle in self._obstacles.values():
                if obstacle.collide(source.get_position(), radius=settings.OBJECT_RADIUS):
                    # Collision
                    collide = True
                    break

        # Update the mapping between position and source as well
        self._sources.update({source.get_position(): source})

        # All went well
        return True

    def remove_source(self, position):
        """
        Remove a given source from the environment.
        :param position: A tuple representing the source's position along the X and Y axis.
        :return: Nothing.
        """

        # Remove the source from the mapping if it is in still in there
        source = self._sources.pop(position, None)
        # Delete the object in memory
        del source

    def add_robot(self, rbt_inst):
        """
        Add a robot to the environment if it does not collide with any obstacle or other robot.
        And finally send 'init' message to robot child.
        :param rbt_inst: A pre-initialized instance of the Robot class.
        :return: Robot. A fully initialized instance of the Robot class.
        """

        # Initialize the collide check
        collide = True

        # Add a robot that does not collide with any obstacle or other robot
        while collide:
            # Get a new robot
            robot = Cylinder(pos_x=random.uniform(settings.MIN_X, settings.MAX_X),
                             pos_y=random.uniform(settings.MIN_Y, settings.MAX_Y), radius=settings.ROBOT_BODY_RADIUS)
            collide = False
            # Check if the robot collides with any obstacle
            for obstacle in self._obstacles.values():
                if obstacle.collide(robot.get_position(), settings.ROBOT_BODY_RADIUS):
                    collide = True
                    break
            if not collide:
                # Check if the robot collides with any other robot
                for rbt_pos in self._robots_position.values():
                    if robot.collide(rbt_pos, settings.ROBOT_BODY_RADIUS):
                        collide = True
                        break

        # No collision detected. Add the robot to the environment
        self._robots_position[rbt_inst] = robot.get_position()
        # Initialize the action tendency with a default value of 0
        self._robots_action_tendency[rbt_inst] = 0

        # Set the initial position of the robot
        rbt_inst.set_position(*robot.get_position())

        # All went well
        return rbt_inst

    def remove_robot(self, rbt):
        """
        Remove a given robot from the environment.
        :param rbt: An instance of the robot class.
        :return: Boolean. True if removal was correctly completed. False otherwise.
        """

        try:
            # Remove the robot from the mapping if it was still in there
            self._robots_position.pop(rbt)
            self._robots_action_tendency.pop(rbt)

            return True
        except KeyError:
            self._logger.error("An error occurred while trying to remove the robot {}, from the set.".format(rbt))
            # Well that happened better let everyone know about it
            return False

    def empty(self):
        """
        Remove all the obstacles and sources from the environment, to free up memory.
        :return: Nothing.
        """

        # Remove all the remaining resources
        for src_pos in list(self._sources.keys()):
            self.remove_source(src_pos)

        # Remove all the remaining obstacles
        for obs_pos in list(self._obstacles.keys()):
            self.remove_obstacle(obs_pos)
