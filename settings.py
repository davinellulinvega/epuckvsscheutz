#!/usr/bin/python
# -*- coding: utf-8 -*-
import os
from math import sqrt

BASE_DIR = os.path.dirname(os.path.abspath(__file__))
PRIMEMO_PATH = os.path.dirname(os.path.abspath(__file__))

ROBOT_POP_SIZE = 10
SOURCE_POP_SIZE = 10
OBSTACLE_POP_SIZE = 10
GPU_MEMORY_RATIO = 1

# Simulation features
# TIME_STEP = 0.05
MAX_NB_CYCLES = 2500
MIN_FLEE_CYCLE = 5
MAX_FLEE_CYCLE = 10

# Robot features
DEFAULT_ROBOT_ENERGY_LEVEL = 2000
ROBOT_HORIZON = 300  # How far can the robot see/detect objects in the world (Taken from Scheutz's paper)
ROBOT_MAX_SPEED = 4
ROBOT_CRIT_SPEED = 1
ROBOT_FLEE_SPEED = 7
ROBOT_BODY_RADIUS = 0.037  # Expressed in meters
FORAGE_ENERGY_AMOUNT = 800
FIGHT_ENERGY_AMOUNT = 50
PROCREATE_ENERGY_AMOUNT = 2000
PROCREATE_ENERGY_REQUIRED = 2200
PROCESS_ENERGY_AMOUNT = 1
DEFAULT_AVOID_TENDENCY = -20
DEFAULT_FORAGE_TENDENCY = 20

# Brain features
BRAIN_ADDR = "localhost"
BRAIN_PORTS = [x for x in range(5360, 5360+ROBOT_POP_SIZE)]
SRV_TIMEOUT = 120.0  # The timeout in seconds

# Environment features
OBSTACLE_THRES = 0.5
FIGHT_THRES = 0.3
OBJECT_RADIUS = 0.05  # Expressed in meters
MIN_X = -900
MAX_X = 900
MIN_Y = -900
MAX_Y = 900
#MIN_X = -900 / sqrt(50/ROBOT_POP_SIZE)  # The maximum position along the X axis (in meters)
#MAX_X = 900 / sqrt(50/ROBOT_POP_SIZE)  # The maximum position along the X axis (in meters)
#MIN_Y = -900 / sqrt(50/ROBOT_POP_SIZE)  # The maximum position along the Y axis (in meters)
#MAX_Y = 900 / sqrt(50/ROBOT_POP_SIZE)  # The maximum position along the Y axis (in meters)
CRITICAL_ENERGY_LVL = 400
DEFAULT_ENERGY_SOURCE_LEVEL = 800

# ProtoEmo features
EXT_IN_LAYER_SIZE = 4
INT_IN_LAYER_SIZE = 3
THAL_LAYER_SIZE = 20
HYPO_LAYER_SIZE = 5
LAT_HYPO_LAYER_SIZE = 10
BA_LAYER_SIZE = 10
LA_LAYER_SIZE = 5
ITC_LAYER_SIZE = 10
CA_LAYER_SIZE = 10
OUT_LAYER_SIZE = 4

# Genetic Algorithm features
POPULATION_SIZE = 20
SPECIES_SIZE = 8  # Set to the number of weight matrices that learn through GA
GA_CHECKPOINT_FILENAME = os.path.join(BASE_DIR, "deap_checkpoint_leaky.pkl")
GA_LOGBOOK_FILENAME = os.path.join(BASE_DIR,"logs", "deap_logbook_leaky.pkl")
MAX_GENERATION = 200

LOGGING = {
    'version': 1,
    'disable_existing_loggers': False,
    'formatters': {
        'simple': {
            'format': "%(asctime)s %(filename)s - %(levelname)s in %(module)s.%(funcName)s at line %(lineno)d:"
                      " %(message)s"
        },
        'compact': {
            'format': "%(asctime)s - %(levelname)s: %(message)s"
        },
        'data': {
            'format': "%(message)s"
        },
    },
    'handlers': {
        'console': {
            'level': 'WARNING',
            'class': 'logging.StreamHandler',
            'formatter': 'simple'
        },
        'supervisor': {
            'level': 'DEBUG',
            'class': 'logging.FileHandler',
            'filename': os.path.join(BASE_DIR, "logs", "supervisor.log"),
            'formatter': 'data'
        },
        'world': {
            'level': 'DEBUG',
            'class': 'logging.FileHandler',
            'filename': os.path.join(BASE_DIR, "logs", "world.log"),
            'formatter': 'simple'
        },
        'robots': {
            'level': 'WARNING',
            'class': 'logging.FileHandler',
            'filename': os.path.join(BASE_DIR, "logs", "robots.log"),
            'formatter': 'data'
        },
        'objects': {
            'level': 'DEBUG',
            'class': 'logging.FileHandler',
            'filename': os.path.join(BASE_DIR, "logs", "objects.log"),
            'formatter': 'compact'
        },
    },
    'loggers': {
        '': {
            'handlers': ['console'],
            'level': 'WARNING'
        },
        'Supervisor': {
            'handlers': ['supervisor', 'console'],
            'level': 'DEBUG',
            'propagate': False,
        },
        'World': {
            'handlers': ['world'],
            'level': 'DEBUG',
            'propagate': False,
        },
        'Robots': {
            'handlers': ['robots'],
            'level': 'WARNING',
            'propagate': False,
        },
        'Objects': {
            'handlers': ['objects'],
            'level': 'DEBUG',
            'propagate': False,
        },
    },
}
