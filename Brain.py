#!/usr/bin/python
# -*- coding: utf-8 -*-
from thespian.actors import ActorTypeDispatcher
from EmergentInterface import IEmergent
from utils import extract_msg, format_msg
from settings import BRAIN_ADDR


class Brain(ActorTypeDispatcher):
    """
    Define a simple actor that receives inputs, activates the brain and sends back the output to the corresponding
    sending actor.
    """

    def __init__(self):
        """
        Initialize the super class and declare the interface to the emergent server.
        """

        # Initialize the super class
        super(Brain, self).__init__()

        # Emergent interface
        self._emer_iface = None

    def receiveMsg_dict(self, message, sender):
        """
        Define the process to execute depending on the action.
        :param message: A dictionary containing the action and its data, if necessary.
        :param sender: The ActorAddress of the Actor sending the message. Probably the LoadBalancer.
        :return: Nothing.
        """

        # Extract the action and its data
        action, data = extract_msg(message)

        if action == 'init':
            # Get the attributes required for establishing a connection to the emergent server.
            host = data.get('host', BRAIN_ADDR)
            port = data.get('port')

            # Establish the connection
            self._emer_iface = IEmergent(host=host, port=port)

            # And that is it for this phase
            self.send(sender, format_msg('ready', data={'address': self.myAddress}))

        elif action == 'activate':
            # Extract the data required
            formatted_data = data.get('formatted_data')
            reply_addr = data.get('sender')

            # Further format the input to conform to the emergent interface
            formatted_inputs = self._emer_iface.format(formatted_data)

            # Activate brain and gather output
            success, data = self._emer_iface.activate(formatted_inputs)

            # Make sure activation was successful
            assert success, data

            # Send the result at the reply address
            self.send(reply_addr, format_msg('results', data={'outputs': data}))

            # Have the brain learn
            self._emer_iface.learn(formatted_inputs)

    def receiveMsg_ActorExitRequest(self, message, sender):
        """
        Simply close the connection to the emergent server and terminates the process.
        :param message:
        :param sender:
        :return: Nothing.
        """

        # Simultaneously close and terminate the emergent server
        self._emer_iface.close()
