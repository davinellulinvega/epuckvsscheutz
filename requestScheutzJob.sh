#!/bin/bash

# Request real and virtual memory
#$ -l mem=16G -l rmem=8G
#
# Ask for an openmpi infini-band environment with 16 cpu
#$ -pe openmp 16
#
# Email notification
#$ -M lcaspar1@sheffield.ac.uk
#
# When to send notifications (beginning, end and abort).
#$ -m bea
#
# Specify the working direcory
#$ -wd /home/acp14lc/Workspaces/EpuckVsScheutz/
#
# Kill it before he names it
#$ -N EpuckVsScheutz

# Load all the modules
module load dev/gcc
module load apps/python/anaconda3-4.2.0
module load apps/python/conda

# Activate the epuckVsScheutz conda environment
source activate epuckVsScheutz

# Launch both scripts
python3 Supervisor.py -f Scheutz -o 50 -s 50 -r 50 --sims 40
