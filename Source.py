#!/usr/bin/python
# -*- coding: utf-8 -*-
from settings import OBJECT_RADIUS, DEFAULT_ENERGY_SOURCE_LEVEL
from Cylinder import Cylinder


class Source(Cylinder):
    """
    Define a cylindrical object with a position and a level of energy.
    """

    def __init__(self, identifier, pos_x, pos_y, energy=DEFAULT_ENERGY_SOURCE_LEVEL):
        """
        Define and initialize the different attributes related to an energy source.
        :param identifier: An integral number identifying the instance.
        :param pos_x: The position of the source along the X axis.
        :param pos_y: The position of the source along the Y axis.
        :param energy: A float representing the initial level of energy.
        """

        # Initialize the Cylinder parent class
        super(Source, self).__init__(pos_x=pos_x, pos_y=pos_y, radius=OBJECT_RADIUS)

        # Initialize the rest of the attributes with the given values
        self._id = identifier
        self._energy = energy

    def decrease_energy(self, units):
        """
        Decrease the energy level by a given amount of units.
        :param units: The amount of energy to forage from the source.
        :return: Float. The number of energy units actually foraged from the the source.
        """

        # Decrease the source's energy level
        foraged = min(units, self._energy)
        self._energy -= foraged

        # Return the amount of foraged energy
        return foraged

    def is_empty(self):
        """
        Check if the source still contains energy or if every last drop has been foraged.
        :return: Boolean. True if no energy left. False otherwise.
        """

        # Has the energy been depleted?
        if self._energy <= 0:
            return True

        # Otherwise
        return False

    def get_id(self):
        """
        A getter for the source identifier.
        :return: Int. The id of the source.
        """

        return self._id
