#!/usr/bin/python
# -*- coding: utf-8 -*-
import os
from math import hypot
import numpy as np
from Robot import Robot
from Cylinder import Cylinder
import settings


class ProtoEmoTf(Robot):
    """
    A child inheriting from the Robot class. It is controlled by the ProtoEmo architecture, implemented within the
    TensorFlow framework, for ease of use on both CPU and GPU, as either local or remote task.
    """

    def __init__(self, id_rbt, ext_in, int_in, rec_ca_in, ca_layer, out_layer, session, gen, spec_idx, ind_idx,
                 pos_x=0, pos_y=0, supervisor=None):
        """
        Initialize the parent class and robot's internal state, as well as define the controlling network model, given
        the individuals.
        :param id_rbt: An integer identifying the robot in this simulation.
        :param ext_in: A tensor representing the external input.
        :param int_in: A tensor representing the internal input.
        :param rec_ca_in: A tensor representing the recurrent CA input.
        :param ca_layer: A tensor representing the CA ouput.
        :param out_layer: A tensor representing the ouput.
        :param session: A tensorflow session.
        :param pos_x: A float representing the robot's initial position along the X axis.
        :param pos_y: A float representing the robot's initial position along the Y axis.
        :param supervisor: An instance of the Supervisor class, managing the current simulation.
        :param gen: An integer representing the current generation.
        :param spec_idx: An integer representing the index of the current specie.
        :param ind_idx: An integer representing the index of the current individual.
        """

        # Initialize the parent's class and by extension the robot's internal state
        super(ProtoEmoTf, self).__init__(id_rbt, pos_x, pos_y, supervisor)

        # Initialize the data log used by the robot to report stats relative to its state for each cycle
        #try:
        #    self._data_log = open(os.path.join(settings.BASE_DIR, "logs", "RobotData",
        #                                       "rbt_{}_{}_{}_{}_{}.log".format(__name__, gen,
        #                                                                       spec_idx, ind_idx, id_rbt)), "w")
        #except IOError as e:
        #    self._error_log.exception("Error: Could not open the log file: {}".format(e))

        # Initialize a variable to keep track of the output of the Central Amygdala layer
        self._ca_activations = np.zeros(shape=settings.CA_LAYER_SIZE, dtype="float32")

        # Initialize the tensors required to compute the tendencies
        self._ext_in = ext_in
        self._int_in= int_in
        self._rec_ca_in = rec_ca_in
        self._ca_layer = ca_layer
        self._out_layer = out_layer

        # Initialize a tensorflow session for this instance
        self._session = session

    def _format(self, sources, obstacles, robots_position, robots_act_tendency):
        """
        Format the robot's internal state and the environment's state to fit the need of the ProtoEmo architecture.
        :param sources: A dictionary mapping the source's position to its instance.
        :param obstacles: A dictionary mapping the obstacle's position to its instance.
        :param robots_position: A bi-dictionary mapping the robot's id to its position and vice versa.
        :param robots_act_tendency: A dictionary mapping the robot's id to its current action tendency.
        :return: A tuple containing the internal and external inputs to the ProtoEmo architecture.
        """

        # Gather the distances to the nearest objects
        obstacle_dist = self._get_dist(self._find_nearest_obstacle(obstacles))
        source_dist = self._get_dist(self._find_nearest_source(sources))
        robot_dist = self._get_dist(self._find_nearest_robot(robots_position))

        # Gather the maximum action tendency from among the nearby robots, if any
        nearby_rbts = self._find_nearest_robot(robots_position, settings.ROBOT_HORIZON)
        if len(nearby_rbts) == 0:
            max_act_tendency = 0
        else:
            max_act_tendency = max([robots_act_tendency[rbt] for rbt in nearby_rbts])

        # Define the set of normalized external inputs
        ext_in = np.array([obstacle_dist / settings.ROBOT_HORIZON,
                           source_dist / settings.ROBOT_HORIZON,
                           robot_dist / settings.ROBOT_HORIZON,
                           max_act_tendency])

        # Compute the level of danger the robot is currently in
        danger = (2 * settings.OBSTACLE_THRES - min(2. * settings.OBSTACLE_THRES, obstacle_dist)) * \
                 (3 * settings.FIGHT_THRES - min(3. * settings.FIGHT_THRES, robot_dist))

        # Define the set of internal inputs
        # The energy level is normalized to a value between 0 and 1, for most cases. It can go over 1, but this case is
        # taken care of by the softsign() activation function on the input layer
        int_in = np.array([self._energy/settings.PROCREATE_ENERGY_REQUIRED,
                           danger, self._tendencies['action']['current']])

        # Return both sets of inputs
        return ext_in, int_in

    def _get_dist(self, cylinder):
        """
        Compute the distance between the robots and the cylinder given in parameter.
        :param cylinder: An instance of the Cylinder class, representing an object on the map (obstacle, source, robot).
        :return: Float. The distance separating the cylinder and the robot.
        """

        # If the cylinder is not a child of the Cylinder class return None
        if not isinstance(cylinder, Cylinder):
            return settings.ROBOT_HORIZON

        # Get the cylinder's position
        cyl_x, cyl_y = cylinder.get_position()

        # Return the distance between robot and cylinder
        return hypot(cyl_x - self._pos_x, cyl_y - self._pos_y)

    def _get_tendencies(self, sources, obstacles, robots_position, robots_act_tendency):
        """
        Retrieve and format the neural network's inputs, and activate it. The resulting ouputs are the action tendencies.
        :param sources: A dictionary mapping the source's position to its instance.
        :param obstacles: A dictionary mapping the obstacle's position to its instance.
        :param robots_position: A bi-dictionary mapping the robot's id to its position and vice versa.
        :param robots_act_tendency: A dictionary mapping the robot's id to its current action tendency.
        :return: A tuple, containing the four tendencies.
        """

        # Format the robot's and environment's states
        ext_in, int_in = self._format(sources, obstacles, robots_position, robots_act_tendency)

        # Within a TensorFlow session, evaluate both the output and the activation of the central amygdala layer
        self._ca_activations, output_acts = self._session.run([self._ca_layer, self._out_layer],
                                                              feed_dict={self._ext_in: ext_in, self._int_in: int_in,
                                                                         self._rec_ca_in: self._ca_activations})

        # Compute the final tendencies
        action_tend = output_acts[0]
        fight_tend = 100. * output_acts[1] - 50
        avoid_tend = settings.DEFAULT_AVOID_TENDENCY * output_acts[2]
        forage_tend = settings.DEFAULT_FORAGE_TENDENCY * output_acts[3]

        # Return the tendencies
        return action_tend, fight_tend, avoid_tend, forage_tend

    def _next_action(self, sources, robots_position, robots_act_tendency, obstacles, cycle):
        """
        Overload the _next_action method from the parent class, to include the computation of the tendencies.
        :param sources: Dict. The mapping between positions and sources.
        :param robots_position: Dict. The mapping between positions and robots.
        :param robots_act_tendency: Dict. The mapping between robots and their respective action tendencies.
        :param obstacles: Dict. The mapping between positions and obstacles.
        :param cycle: Int. An integer representing the current simulation cycle.
        :return: Nothing.
        """

        # Compute the new tendencies given the robot's and environment's states
        action_tend, fight_tend, avoid_tend, forage_tend = self._get_tendencies(sources, obstacles, robots_position,
                                                                                robots_act_tendency)

        # Assign the new tendencies
        self._tendencies['action']['current'] = action_tend
        self._tendencies['avoid'] = avoid_tend
        self._tendencies['fight'] = fight_tend
        self._tendencies['forage'] = forage_tend

        # Call on the _next_action method from the parent class to decide on our next move
        super(ProtoEmoTf, self)._next_action(sources, robots_position, robots_act_tendency, obstacles, cycle)
