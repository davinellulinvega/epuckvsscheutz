#!/usr/bin/python3
# -*- coding: utf-8 -*-
import ujson
import socket
import logging
import os
from time import sleep
from subprocess import Popen, PIPE
from settings import BASE_DIR, LOGGING, BRAIN_ADDR, PRIMEMO_PATH


def valid_json(s):
    """
    Check if a given string is a valid json string.
    :param s: A string.
    :return: Boolean. True if string is valid json, False otherwise.
    """

    try:
        # Simply load the string and hope for the best
        ujson.loads(s)
    except ValueError:
        # But be prepared for the worse
        return False
    # If no exception is raised, it was a valid json string
    return True


class IEmergent:
    """
    Defines an interface between the bot communicating with the server over http and the Emergent software (the brain),
    using the TCP/IP protocol.
    """

    def __init__(self, host=BRAIN_ADDR, port=5360):
        """
        Initialize the socket, logger and other required attributes for speaking to the emergent software.
        :param host: Hostname of the Emergent server. Defaults to: localhost
        :param port: Port on which the Emergent server is listening. Defaults to: 5360
        """

        # Declare a logger
        self._log = logging.getLogger(__name__)
        handler = logging.FileHandler(os.path.join(BASE_DIR, "logs", "brain_{}_{}.log".format(host, port)))
        handler.setFormatter(logging.Formatter(LOGGING['formatters']['compact']['format']))
        self._log.addHandler(handler)
        self._log.setLevel(logging.DEBUG)
        self._process = Popen([
            'emergent', '--nogui', '--server', '--port', str(port), '--proj', os.path.join(PRIMEMO_PATH, "primEmo.proj")
        ], stdin=PIPE, stdout=PIPE, stderr=PIPE)

        # Check if the process is running correctly
        returncode = self._process.poll()
        if returncode is not None:
            raise RuntimeError("The process associated with the Emergent program"
                               " does not seem to be alive anymore. Returned code: {}, "
                               "which is also the terminating signal.".format(returncode))

        # Wait a little bit for the process to initialize
        sleep(20)

        # Open a socket for communication
        self._connect(host, port, 5)

        # Flush the welcome banner
        data = self._recv(255)
        self._log.debug("Welcome banner flushed: {}".format(data))

    def _connect(self, host, port, retry):
        """
        Connect a TCP socket to the emergent server. If connection fails, retries a given number of times.
        :param host: A string representingg the ip address or hostname of emergent's server.
        :param port: The port number on which emergent is listening.
        :param retry: A integer indicating how many connection attempts should be made before giving up.
        :return: Nothing.
        """

        # Try to open the connection
        try:
            self._sock = socket.create_connection((host, port), timeout=30)
        except socket.timeout:
            # Write a message in the logs
            self._log.exception("The connection to Emergent ({}:{}) timed out.".format(host, port))
            # Check if the process is still alive and we should retry
            if self._process.poll() is None and retry > 0:
                # Wait a second
                sleep(1)
                # Have another go
                self._connect(host, port, retry-1)
            else:
                # Raise a TimeoutError exception
                raise TimeoutError("The connection timed out.")
        except socket.error:
            # Write a message in the logs
            self._log.exception("An error occurred while creating a"
                                " connection to Emergent ({}:{}).".format(host, port))
            # Check if the process is still alive and we should retry
            if self._process.poll() is None and retry > 0:
                # Wait a second
                sleep(1)
                # Have another go
                self._connect(host, port, retry-1)
            else:
                # Raise an AttributeError exception for the user to know something went wrong
                raise AttributeError("An error occurred while creating a connection.")

    def _recv(self, buff_size=2048):
        """
        Receive data from the server. And handle all likely to occur exceptions.
        :param buff_size: The maximum size of the buffer to use in bytes.
        :return: Bytes. The data received from the server.
        """

        try:
            # Get data from the server
            data = self._sock.recv(buff_size)
        except InterruptedError:
            # The connection was cut in the middle of receiving
            self._log.exception("Connection was interrupted while receiving data.")
            # close the socket
            self.close()
            # Propagate the exception
            raise InterruptedError("The connection to the Emergent server was interrupted.")
        except socket.timeout:
            # Write a message in the logs
            self._log.exception("The connection to Emergent timed out.")
            # close the socket
            self.close()
            # Raise a TimeoutError exception
            raise TimeoutError("The connection timed out.")
        except socket.error as e:
            # Write a message in the logs
            self._log.exception("An error occurred while receiving data from Emergent.")
            # close the socket
            self.close()
            # Raise a RuntimeError exception for the user to know something went wrong
            raise RuntimeError("An error occurred while receiving data: {}".format(e))

        # Return the received data
        return data

    def _send(self, data):
        """
        Simply send data to the server. And handle all likely to occur exceptions.
        :param data: The data to send.
        :return: Nothing.
        """

        # Send the data until nothing is left
        orig_len = len(data)
        tot_sent = 0
        while tot_sent < orig_len:
            try:
                # Send data to the server
                sent = self._sock.send(data[tot_sent:])
            except InterruptedError:
                # The connection was cut in the middle of receiving
                self._log.exception("Connection was interrupted while sending data.")
                # close the socket
                self.close()
                # Propagate the exception
                raise InterruptedError("The connection to the Emergent server was interrupted while sending data.")
            except socket.timeout:
                # Write a message in the logs
                self._log.exception("The connection to Emergent timed out.")
                # close the socket
                self.close()
                # Raise a TimeoutError exception
                raise TimeoutError("The connection timed out.")
            except socket.error as e:
                # Write a message in the logs
                self._log.exception("An error occurred while sending data from Emergent.")
                # close the socket
                self.close()
                # Raise a RuntimeError exception for the user to know something went wrong
                raise RuntimeError("An error occurred while sending data: {}".format(e))

            # If no other exception has been raised, but still no data is sent
            assert sent != 0, "An error occurred while sending data to Emergent."

            # Increase the total amount of sent data
            tot_sent += sent

    def _recv_json(self, buff_size=2048):
        """
        Receive valid json formatted data from the server.
        :param buff_size: The maximum size of the buffer to use in bytes.
        :return: Object. A python object corresponding to the decoded json formatted data.
        """

        # Make sure the socket is connected to something
        assert self._sock is not None, "Unable to receive data on a non-connected socket."

        data = ''
        # Get data from the server until everything has been received
        while not valid_json(data):
            data += self._recv(buff_size=buff_size).decode()

        # Decode and return the json formatted data
        return ujson.loads(data)

    def _send_json(self, obj):
        """
        Send a json formatted string to the server.
        :param obj: A python object representing a command to be sent.
        :return: Nothing.
        :exception ValueError: Raised if the json_str argument is not correctly encoded in json.
        """

        # Make sure the socket is connected to something
        assert self._sock is not None, "Unable to receive data on a non-connected socket."

        # Format the command
        cmd = "{}\n".format(ujson.encode(obj))

        # Send the data to the server
        self._send(cmd.encode())

    def close(self):
        """
        Close the socket.
        :return: Nothing.
        """

        # Check that the socket has been open before closing
        if self._sock is not None:
            # First shut it down
            self._sock.shutdown(socket.SHUT_RDWR)
            # Then free all associated resources
            self._sock.close()
            # Reinitialize the socket making it unusable
            self._sock = None

        # Check if the emergent process is running
        if self._process is not None:
            # Terminate the process
            self._process.terminate()
            # Wait for the process to gracefully stop
            try:
                self._process.wait(timeout=20)
            except TimeoutError:
                # Unless you have to wait for too long, then kill the process forcefully
                self._process.kill()

    def format(self, state):
        """
        Format the data received to describe the bot's state into a data row that can be sent to Emergent.
        :param state: A dictionary containing the required data.
        :return: Dict. A dictionary formatted following Emergent's standards.
        """

        # Extract the tendencies
        tendencies = state.get('tendencies', {'action': 0, 'fight': 0, 'avoid': 0, 'forage': 0})

        # Format the data part of the command with the given state
        format_state = {
            "columns": [
                {
                    "dimensions": [
                        4,
                        1
                    ],
                    "matrix": "true",
                    "name": "Output",
                    "type": "float",
                    "values": [
                        [
                            [
                                tendencies['action'],
                                tendencies['fight'],
                                tendencies['avoid'],
                                tendencies['forage']
                             ]
                        ]
                    ]
                },
                {
                    "dimensions": [
                        4,
                        1
                    ],
                    "matrix": "true",
                    "name": "Input",
                    "type": "float",
                    "values": [
                        [
                            [
                                state.get('distances').get('source', 0),
                                state.get('distances').get('robot', 0),
                                state.get('distances').get('obstacle', 0),
                                state.get('max_action_tendency', 0)
                            ]
                        ]
                    ]
                },
                {
                    "dimensions": [
                        1,
                        1
                    ],
                    "matrix": "true",
                    "name": "NegBodyState",
                    "type": "float",
                    "values": [
                        [[state.get('danger', 0)]]
                    ]
                },
                {
                    "dimensions": [
                        1,
                        1
                    ],
                    "matrix": "true",
                    "name": "PosBodyState",
                    "type": "float",
                    "values": [
                        [[state.get('energy', 0)]]
                    ]
                },
                {
                    "dimensions": [
                        1,
                        1
                    ],
                    "matrix": "true",
                    "name": "CurrActionTend",
                    "type": "float",
                    "values": [
                        [[state.get('curr_action_tendency', 0)]]
                    ]
                }
            ]
        }

        # Return the formatted state
        return format_state

    def _has_failed(self):
        """
        Check if a command send to the server has failed or not
        :return: Boolean, String. True if the command failed, False otherwise. In case of failure, String contains the
        error message returned by emergent.
        """

        # Get the result
        data = self._recv_json()

        # Check if there are any errors
        if data['status'] == "ERROR":
            return True, "Error {}: {}".format(data['error'], data['message'])

        # If no error detected return true
        return False, None

    def activate(self, state):
        """
        Call the activation program, for a given state, on the brain. 
        No learning occur during this call.
        :param state: A matrix formatted representation of a given player state.
        :return: Boolean, List/String. True if the operation went according to plan, False otherwise.
        A list containing the activation values of the output layer after the network has settled in the minus phase.
        In case the command fails, List will be replaced by a String containing the error message.
        """

        # Set the input data
        cmd = {
            "command": "SetData",
            "table": "RobotState",
            "data": state
        }

        # Send the command
        self._send_json(cmd)
        # Check for any error
        failed, msg = self._has_failed()
        if failed:
            return False, msg

        # Ask the server to activate
        cmd = {
            "command": "RunProgram",
            "program": "Activate"
        }
        self._send_json(cmd)

        # Check if there were any errors
        failed, msg = self._has_failed()
        if failed:
            return False, msg

        # Get the result of the activation
        cmd = {
            "command": "GetData",
            "table": "Tendencies"
        }

        # Send the command to the server
        self._send_json(cmd)

        # Get the activation values
        data = self._recv_json()
        if data['status'] == "ERROR":
            return False, "Error {}: {}".format(data['error'], data['message'])

        # Compile the data into a list
        values = data['result']['columns'][0]['values'][0][0]  # No it is not complex nor is it stupid
        act_vals = [float(row) for row in values]

        # Return the activation values
        return True, act_vals

    def learn(self, state):
        """
        Call the learn program, for a given state and corresponding target output. on the brain.
        :param state: A matrix formatted representation of a given player state.
        :return: Boolean, String. True if no error where observed, False otherwise. If False, String will contain
        details regarding the error.
        """

        # Set the input data
        cmd = {
            "command": "SetData",
            "table": "RobotState",
            "data": state
        }
        self._send_json(cmd)

        # Check for any error
        failed, msg = self._has_failed()
        if failed:
            return False, msg

        # Ask the server to activate
        cmd = {
            "command": "RunProgram",
            "program": "Learn"
        }
        self._send_json(cmd)

        # Check if there were any errors
        failed, msg = self._has_failed()
        if failed:
            return False, msg

        # Everything went smoothly
        return True, None

    def save_logs(self):
        """
        Instruct the emergent server to record all trial level stats to a file for further analysis.
        :return: Boolean, String. True if the command executed properly, False otherwise. In case of failure, String 
        will contain details concerning the error.
        """

        # Send a command to trigger the save log files program
        cmd = {
            "command": "RunProgram",
            "program": "SaveLogFiles"
        }
        self._send_json(cmd)

        # Check if the command failed
        failed, msg = self._has_failed()
        if failed:
            return False, msg

        # If everything went according to plan
        return True, None

if __name__ == "__main__":
    # Create a simple interface to the default server (localhost:5360)
    brain = IEmergent(host=BRAIN_ADDR, port=5360)

    try:
        # Send a bogus command
        brain._send_json({'command': 'GetData', 'table': 'RobotState'})

        # Receive any response
        res = brain._recv_json()

        # Close the connection
        brain.close()

        print("Received: {}".format(res))
    except KeyboardInterrupt:
        print("Closing")
