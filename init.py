#!/usr/bin/python
# -*- coding: utf-8 -*-
import datetime
import logging.config
import argparse
import settings
from Supervisor import Supervisor
import Genetic
from Scheutz import Scheutz

# Define all arguments required to run the supervisor from the command line
parser = argparse.ArgumentParser()
parser.add_argument("-f", type=str, dest="factory", help="The factory to use to populate the world with robots. "
                                                         "Available: Scheutz and ProtoEmoTf.", metavar="factory",
                    required=True, choices=['Scheutz', 'ProtoEmoTf'])
parser.add_argument("--sims", type=int, dest="nb_sims", required=False, help="The number of simulations to run.",
                    default=1)
parser.add_argument("--aws", dest="aws", required=False, help="Whether the code is to be run an Amazon Web Service "
                                                              "instance", action="store_true")
parser.add_argument("--start_at", type=int, dest="start_sim", required=False, help="Give the simulation number to "
                                                                                   "start on.", default=0)

# Initialize the logging module
logging.config.dictConfig(settings.LOGGING)

# Parse the arguments
args = parser.parse_args()

if args.factory == 'Scheutz':
    rbt_factory = Scheutz

    # As long as there are simulations to perform
    nb_sim = args.start_sim
    while nb_sim < args.nb_sims + args.start_sim:
        # Initialize the supervisor
        supervisor = Supervisor(rbt_factory, nb_sim)
        start_time = datetime.datetime.now()
        # Run the supervisor
        supervisor.run()
        end_time = datetime.datetime.now()
        # Increase the number of simulations run so far
        nb_sim += 1

        print("Execution time: {}".format(end_time - start_time))

elif args.factory == 'ProtoEmoTf':
    # Directly launch the genetic algorithm
    Genetic.main(on_aws=args.aws)

else:
    logging.error("An invalid or no factory was provided.\n Terminating the simulation.")
    exit(-1)
